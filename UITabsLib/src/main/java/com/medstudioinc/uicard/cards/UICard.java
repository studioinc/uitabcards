package com.medstudioinc.uicard.cards;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;



import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.RequestOptions;
import com.medstudioinc.uicard.utils.DisplayUtility;
import com.medstudioinc.uicard.utils.UICardOptions;
import com.medstudioinc.uicard.widget.MaterialRippleLayout;
import com.medstudioinc.uicard.widget.UICardRoundedLayout;
import com.medstudioinc.uitabslib.R;
import com.medstudioinc.uicard.listener.OnCardViewClickListener;
import com.medstudioinc.uicard.listener.OnCardViewTouchListener;
import com.medstudioinc.uicard.listener.OnLongCardViewClickListener;
import com.medstudioinc.uicard.utils.GradientColors;


public class UICard extends ConstraintLayout {
    public ImageView cardThumbnail;

    public TextView cardTitle;

    private ConstraintLayout constraintLayout;
    private MaterialRippleLayout cardRippleLayout;
    public UICardRoundedLayout cardRoundedCornerView;

    private GradientColors gradientColors;

    private Drawable cardThumbnailRes;
    private String cardTitleText;
    private int cardTitleColor;
    private float cardTitleSize;
    private Typeface cardTitleTypeface;
    private int cardBackgroundSolidColor;

    private float cardRoundedCornersRadius;

    private int blurRadius;
    private int blurDownScaleFactor;
    private boolean blurAllowFallback;
    private boolean blurDebug;
    private int blurOverlayColor;

    private boolean cardHasBluring;
    private boolean thumbnailColorFilter;

    private int cardWidth;
    private int cardHeight;

    private OnLongCardViewClickListener longCardViewClickListener;
    private OnCardViewClickListener cardViewClickListener;
    private OnCardViewTouchListener cardViewTouchListener;

    private UICardOptions uiCardOptions;

    private Paint grayscalePaint;
    private ColorMatrix colorMatrix;

    public UICard(Context context) {
        super(context);
    }

    public UICard(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UICard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.defaultAttrs(context, attrs, defStyleAttr);
    }

    private void defaultAttrs(Context context, AttributeSet attrs, int defStyleAttr){
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CardLayerView, defStyleAttr, defStyleAttr);
        try {
            this.cardTitleText = typedArray.getString(R.styleable.CardLayerView_cardTitleText);
            this.cardTitleColor = typedArray.getColor(R.styleable.CardLayerView_cardTitleColor,0);
            this.cardTitleSize = DisplayUtility.dp2px(context, typedArray.getResourceId(R.styleable.CardLayerView_cardTitleSize, 0));
            this.cardBackgroundSolidColor = typedArray.getColor(R.styleable.CardLayerView_cardBackgroundSolidColor,0);
            this.cardRoundedCornersRadius = typedArray.getDimensionPixelSize(R.styleable.CardLayerView_cardRoundedCornersRadius,0);
        } finally {
            typedArray.recycle();
        }

        isInEditMode();
        views();
    }

    private void defaultValues(){
        this.cardTitleText = this.uiCardOptions.getTitleText();
        this.cardTitleSize = this.uiCardOptions.getTitleSize();
        this.cardTitleColor = this.uiCardOptions.getTitleColor();
        this.cardRoundedCornersRadius = this.uiCardOptions.getRoundedCornersRadius();
        this.cardThumbnailRes = this.uiCardOptions.getThumbnailRes();

        setCardThumbnail(cardThumbnailRes);

    }

    public void views(){
        this.constraintLayout = new ConstraintLayout(getContext());
        ConstraintLayout.LayoutParams rootViewParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        this.constraintLayout.setLayoutParams(rootViewParams);


        this.cardRippleLayout = new MaterialRippleLayout(getContext());
        this.cardRippleLayout.setId(R.id.card_ripple_layout);
        FrameLayout.LayoutParams rippleParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        this.cardRippleLayout.setLayoutParams(rippleParams);


        this.cardRoundedCornerView = new UICardRoundedLayout(getContext());
        this.cardRoundedCornerView.setId(R.id.rounded_corner_view);
        FrameLayout.LayoutParams roundedCornerParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
       /* roundedCornerParams.topMargin = this.uiCardOptions.getUiCardLayoutParams().getTop();
        roundedCornerParams.leftMargin = this.uiCardOptions.getUiCardLayoutParams().getLeft();
        roundedCornerParams.bottomMargin = this.uiCardOptions.getUiCardLayoutParams().getBottom();
        roundedCornerParams.rightMargin = this.uiCardOptions.getUiCardLayoutParams().getRight();
        roundedCornerParams.width = this.uiCardOptions.getCardMinWidth();
        roundedCornerParams.height = this.uiCardOptions.getCardMinHeight();*/
        roundedCornerParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
        roundedCornerParams.height = DisplayMetrics.DENSITY_HIGH;
        this.cardRoundedCornerView.setLayoutParams(roundedCornerParams);


        this.cardThumbnail = new ImageView(getContext());
        this.cardThumbnail.setId(R.id.card_thumbnail);
        ViewGroup.LayoutParams thumbnailParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        this.cardThumbnail.setLayoutParams(thumbnailParams);


        /*this.cardBlurringView = new BlurringView(getContext());
        this.cardBlurringView.setId(R.id.blurring_view);
        ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        this.cardBlurringView.setLayoutParams(params);
        this.cardBlurringView.setVisibility(GONE);
*/

        this.cardTitle = new TextView(getContext());
        ConstraintLayout.LayoutParams txtParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        txtParams.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
        txtParams.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID;
        txtParams.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID;
        txtParams.rightToRight = ConstraintLayout.LayoutParams.PARENT_ID;
        this.cardTitle.setLayoutParams(txtParams);
        this.cardTitle.setId(R.id.card_title);


        this.cardRoundedCornerView.addView(this.cardThumbnail);
        this.cardRoundedCornerView.addView(this.cardTitle);

        this.cardRippleLayout.addView(this.cardRoundedCornerView);
        this.constraintLayout.addView(this.cardRippleLayout);


        this.cardRippleLayout.setOnClickListener(v-> {
            if (this.cardViewClickListener != null) {
                this.cardViewClickListener.onClick(v);
            }
        });
        this.cardRippleLayout.setOnLongClickListener(v-> {
            if (this.longCardViewClickListener != null){
                this.longCardViewClickListener.onLongClick(v);
            }
            return false;
        });
        this.cardRippleLayout.setOnTouchListener((v, e)->{
            if (this.cardViewTouchListener != null){
                this.cardViewTouchListener.onTouch(v, e);
            }
            return false;
        });


        //this.cardRoundedCornerView.setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent));
        this.cardRippleLayout.setRippleDelayClick(true);
        this.cardRippleLayout.setRippleOverlay(true);
        this.cardRippleLayout.setRippleHover(true);
        this.cardRippleLayout.setRippleAlpha(1);
        this.cardRippleLayout.setRippleColor(Color.parseColor("#b5E9EBEE"));

       /* if (this.uiCardOptions.isCardHasBluring()) {
            this.cardRoundedCornerView.addView(this.cardBlurringView, 3);
            this.blurRadius = 2;
            this.blurDownScaleFactor = 1;
            this.blurAllowFallback = false;
            this.blurOverlayColor = android.R.color.transparent;
            this.blurDebug = true;

            this.cardBlurringView.setBlurRadius(this.blurRadius);
            this.cardBlurringView.setDownScaleFactor(this.blurDownScaleFactor);
            this.cardBlurringView.setDebug(this.blurDebug);
            this.cardBlurringView.setAllowFallback(this.blurAllowFallback);
            this.cardBlurringView.setOverlayColor(this.blurOverlayColor);
            BlurSupport.addTo(this.cardRoundedCornerView);
        }*/

        defaultValues();


        addView(this.constraintLayout);
    }


    public void writeOptions(UICardOptions options){
        this.uiCardOptions = options;

        if (this.cardTitle != null){
            this.cardTitle.setShadowLayer(
                    this.uiCardOptions.getTextShadowLayer().getTextShadowRadius(),
                    this.uiCardOptions.getTextShadowLayer().getTextShadowDx(),
                    this.uiCardOptions.getTextShadowLayer().getTextShadowDy(),
                    this.uiCardOptions.getTextShadowLayer().getTextShadowColor());
        }

        if (this.cardTitle != null && this.uiCardOptions.getTitleText() != null){
            this.cardTitle.setText(this.uiCardOptions.getTitleText());
        }

        if (this.cardTitle != null && this.uiCardOptions.getTitleSize() != 0){
            this.cardTitle.setTextSize(this.uiCardOptions.getTitleSize());
        }

        if (this.cardTitle != null){
            this.cardTitle.setTextColor(this.uiCardOptions.getTitleColor());
        }

        if (this.cardTitle != null && this.uiCardOptions.getCardTitleTypeface() != null){
            this.cardTitle.setTypeface(this.uiCardOptions.getCardTitleTypeface());
        }

       /* Toast.makeText(getContext(), this.uiCardOptions.getThumbnailRes().getMinimumHeight()+"", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadThumbnail(uiCardOptions.getThumbnailRes()).into(cardThumbnail);
            }
        }, 1000);
*/



        if (this.uiCardOptions.isThumbnailColorFilter()) {
            grayscalePaint = new Paint();
            colorMatrix = new ColorMatrix();
            colorMatrix.setSaturation(0);
            grayscalePaint.setColorFilter(this.uiCardOptions.getThumbnailColorFilter());
            //grayscalePaint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
            //this.cardThumbnail.setColorFilter(this.uiCardOptions.getThumbnailColorFilter());
            //setGrayedOut(true);
        }


       /* if (this.cardAlpha != null && this.uiCardOptions.getBackgroundSolidColor() != 0){
            this.cardAlpha.setBackgroundColor(this.uiCardOptions.getBackgroundSolidColor());
        }

        if (this.cardAlpha != null && this.uiCardOptions.getGradientColors() != null){
            if (this.uiCardOptions.getGradientColors().length() == 2){
                GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] {
                        Color.parseColor(this.uiCardOptions.getGradientColors().first()),
                        Color.parseColor(this.uiCardOptions.getGradientColors().second())
                });

                //cardThumbnail.setColorFilter(Color.parseCol or(this.gradientColors.second()), PorterDuff.Mode.OVERLAY);
                //cardThumbnail.setColorFilter(gd);

                this.cardAlpha.setBackgroundDrawable(gd);
                this.cardAlpha.setAlpha(0.8f);
                //this.cardAlphaShadow.setBackgroundColor(Color.parseColor(this.gradientColors.first().substring(0, 3) + "000000"));
            }
        }*/

       /* if (this.cardRoundedCornerView != null && this.cardRippleLayout != null) {
            this.cardRoundedCornerView.setRoundedCornerRadius(this.uiCardOptions.getRoundedCornersRadius());
            this.cardRippleLayout.setRippleRoundedCorners((int) this.uiCardOptions.getRoundedCornersRadius());
        }*/




       /* if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRippleDelayClick(this.uiCardOptions.isRippleDelayClick());
        }

        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRadius(this.uiCardOptions.getRippleRadius());
        }

        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRippleOverlay(this.uiCardOptions.isRippleOverlay());
        }

        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRippleHover(this.uiCardOptions.isRippleHover());
        }

        if (this.cardRippleLayout != null){
            if (this.uiCardOptions.getRippleColor() != 0){
                this.cardRippleLayout.setRippleColor(this.uiCardOptions.getRippleColor());
            }
        }

        if (this.cardRippleLayout != null){
            if (this.uiCardOptions.getRippleAlpha() != null){
                this.cardRippleLayout.setRippleAlpha(this.uiCardOptions.getRippleAlpha());
            }
        }*/

       /* if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRippleBackground(this.uiCardOptions.getRippleBackground());
        }

        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRippleDiameter(this.uiCardOptions.getRippleDiameter());
        }

        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRippleFadeDuration(this.uiCardOptions.getRippleFadeDuration());
        }

        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRippleInAdapter(this.uiCardOptions.isRippleInAdapter());
        }

        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRipplePersistent(this.uiCardOptions.isRipplePersistent());
        }

        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setDefaultRippleAlpha(this.uiCardOptions.getDefaultRippleAlpha());
        }*/

        /*if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRippleDuration(this.uiCardOptions.getRippleDuration());
        }
        */

        views();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

    }

    public void setCardThumbnail(Drawable drawable){
        //this.cardThumbnailRes = getResources().getDrawable(drawable);
        if (this.cardThumbnail != null && drawable != null){
            loadThumbnail(drawable).into(cardThumbnail);
            //this.loadThumbnail(this.uiCardOptions.getThumbnailRes()).into(this.cardThumbnail);




            //Bitmap myBitmap = ((BitmapDrawable)this.cardThumbnail.getDrawable()).getBitmap();

            //Bitmap newBitmap = addGradient(myBitmap);
            //this.cardThumbnail.setImageDrawable(new BitmapDrawable(getResources(), newBitmap));

            /*cardThumbnail.buildDrawingCache();
            Bitmap newBitmap = cardThumbnail.getDrawingCache();
            this.cardThumbnail.setImageDrawable(new BitmapDrawable(getResources(), newBitmap));*/
        }
    }

    public Bitmap addGradient(Bitmap originalBitmap) {
        int width = originalBitmap.getWidth();
        int height = originalBitmap.getHeight();
        Bitmap updatedBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(updatedBitmap);

        canvas.drawBitmap(originalBitmap, 0, 0, null);

        Paint paint = new Paint();
        LinearGradient shader = new LinearGradient(0, 0, 0, height, 0xFFF0D252, 0xFFF07305, Shader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawRect(0, 0, width, height, paint);

        return updatedBitmap;
    }

    public void setGrayedOut(boolean grayedOut) {
        if (grayedOut) {
            setLayerType(View.LAYER_TYPE_HARDWARE, grayscalePaint);
        } else {
            setLayerType(View.LAYER_TYPE_NONE, null);
        }
    }


    public View getView() {
        if (this.cardRoundedCornerView != null){
            return this.cardRoundedCornerView;
        }
        return getRootView();
    }

    @Override
    public void removeAllViews() {
        if (this.constraintLayout != null){
            this.constraintLayout.removeAllViews();
        }
        super.removeAllViews();
    }

    /**
     * Provides type independent options to customize loads with Glide.
     */
    public RequestOptions requestOptions(){
        return new RequestOptions();
    }


    /**
     * A helper method equivalent to calling {@link #requestOptions()} and then {@link
     * RequestBuilder#load(Object)} with the given model.
     *
     * @return A new request builder for loading a {@link Drawable} using the given model.
     */

    private RequestBuilder<Drawable> loadThumbnail(@Nullable Object file){
        return Glide.with(getContext()).load(file).apply(requestOptions().centerCrop());
    }

    /**
     * Register a callback to be invoked when this view is clicked. If this view is not
     * clickable, it becomes clickable.
     *
     * @param listener The callback that will run
     *
     * @see #setClickable(boolean)
     */
    public void setOnCardViewClickListener(OnCardViewClickListener listener){
        this.cardViewClickListener = listener;
    }

    /**
     * Register a callback to be invoked when this view is clicked and held. If this view is not
     * long clickable, it becomes long clickable.
     *
     * @param listener The callback that will run
     *
     * @see #setLongClickable(boolean)
     */
    public void setOnCardViewLongClickListener(OnLongCardViewClickListener listener){
        this.longCardViewClickListener = listener;
    }

    public void setOnCardViewTouchListener(OnCardViewTouchListener listener){
        this.cardViewTouchListener = listener;
    }
}

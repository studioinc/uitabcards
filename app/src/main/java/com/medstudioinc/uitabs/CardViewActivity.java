package com.medstudioinc.uitabs;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.medstudioinc.uicard.utils.CornerType;
import com.medstudioinc.uicard.widget.UICardRoundedLayout;

public class CardViewActivity extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        RelativeLayout relativeLayout = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams
                = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT);
        relativeLayout.setLayoutParams(layoutParams);

        relativeLayout.setBackgroundColor(Color.parseColor("#F0F0F0"));


        DisplayMetrics metrics = getResources().getDisplayMetrics();

        float height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, metrics);


        CardView cardView = new CardView(this);
        RelativeLayout.LayoutParams cardParams
                = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        cardView.setLayoutParams(cardParams);
        cardView.setRadius(50);
        cardView.setCardBackgroundColor(Color.parseColor("#F2F1F6"));
        cardView.setCardElevation(40);
        cardView.setUseCompatPadding(true);


        UICardRoundedLayout uiCardRoundedLayout = new UICardRoundedLayout(this);
        uiCardRoundedLayout.setCornerRadius(50, CornerType.TOP_LEFT);

        RelativeLayout.LayoutParams kornerParams
                = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        kornerParams.topMargin = 10;
        kornerParams.leftMargin = 10;
        kornerParams.rightMargin = 10;
        kornerParams.bottomMargin = 10;
        uiCardRoundedLayout.setLayoutParams(kornerParams);


        ImageView imageView = new ImageView(this);
        RelativeLayout.LayoutParams textViewParams
                = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                (int) height);
        imageView.setLayoutParams(textViewParams);
        imageView.setImageResource(R.drawable.sport);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);


        //cardView.addView(imageView);
        //relativeLayout.addView(cardView);

        uiCardRoundedLayout.addView(imageView);
        relativeLayout.addView(uiCardRoundedLayout);



        setContentView(relativeLayout, layoutParams);
    }
}

package com.medstudioinc.uitabs;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.medstudioinc.uicard.UITab;
import com.medstudioinc.uicard.UITabHost;
import com.medstudioinc.uicard.UITabListener;

public class SimpleTabsActivity extends AppCompatActivity implements UITabListener {
    private UITabHost tabHost;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        RelativeLayout relativeLayout = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams
                = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT);
        relativeLayout.setLayoutParams(layoutParams);

        relativeLayout.setBackgroundColor(Color.parseColor("#EFF0F1"));

        tabHost = new UITabHost(this);
        tabHost.setId(R.id.tabs);
        tabHost.setPrimaryColor(Color.TRANSPARENT);
        tabHost.setAccentColor(getResources().getColor(R.color.colorAccent));
        tabHost.setTextColor(Color.BLACK);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 56, metrics);

        RelativeLayout.LayoutParams tabHostParams
                = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.FILL_PARENT,
                (int) height);
        tabHost.setLayoutParams(tabHostParams);

        relativeLayout.addView(tabHost);



        setContentView(relativeLayout, layoutParams);
        addTabs();
    }

    private void addTabs(){
        for (int i = 0; i < getTabTitles().length; i++){
            tabHost.addTab(tabHost.newTab()
                    .setText(getTabTitles()[i])
                    .setTabListener(this));
        }
    }


    private String[] getTabTitles(){
        return new String[] {
                "Coding",
                "Ideas",
                /* "Drink",
                 "Food",
                 "Health",
                 "Lifestyle",
                 "Sport",
                 "Yoga",
                 "Hopital",
                 "Addiction",
                 "Beauty",
                 "Office"*/
        };
    }

    private String[][] getTabColors(){
        return new String[][] {
                {"EB5757", "000000"},
                {"6441A5", "2a0845"},
                {"43cea2", "185a9d"},
                {"fd1d1d", "fcb045"},
                {"c43a30", "874da2"},
                {"FD8BD9", "7742B2"},
                {"243949", "517fa4"},
                {"243949", "517fa4"},
                {"434343", "000000"},
                {"e52d27", "b31217"},
                {"f83600", "f9d423"},
                {"4481eb", "04befe"}
        };
    }

    private int[] getTabThumbnails(){
        return new int[] {
                R.drawable.conding,
                R.drawable.ideas,
                R.drawable.drinks,
                R.drawable.food,
                R.drawable.health,
                R.drawable.lifestyle,
                R.drawable.sport,
                R.drawable.yoga,
                R.drawable.surgery,
                R.drawable.addiction,
                R.drawable.nature,
                R.drawable.office
        };
    }

    @Override
    public void onTabSelected(UITab tab) {
        tabHost.setSelectedNavigationItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(UITab tab) {

    }

    @Override
    public void onTabUnselected(UITab tab) {

    }
}

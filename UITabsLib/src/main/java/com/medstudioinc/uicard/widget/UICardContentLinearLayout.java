package com.medstudioinc.uicard.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;

public class UICardContentLinearLayout extends LinearLayout {
    public UICardContentLinearLayout(Context context) {
        super(context);
    }

    public UICardContentLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public UICardContentLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setOrientation(int orientation) {
        super.setOrientation(VERTICAL);
    }

    @Override
    public void setGravity(int gravity) {
        super.setGravity(Gravity.CENTER);
    }
}

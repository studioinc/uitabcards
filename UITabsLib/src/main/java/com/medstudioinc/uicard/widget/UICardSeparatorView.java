package com.medstudioinc.uicard.widget;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;

public class UICardSeparatorView extends ConstraintLayout {
    public UICardSeparatorView(Context context) {
        super(context);
    }

    public UICardSeparatorView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UICardSeparatorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}

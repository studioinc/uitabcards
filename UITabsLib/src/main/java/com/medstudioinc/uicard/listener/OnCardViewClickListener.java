package com.medstudioinc.uicard.listener;

import android.view.View;

public interface OnCardViewClickListener {
    void onClick(View v);
}

package com.medstudioinc.uicard.widget;

import android.content.Context;
import android.util.AttributeSet;



public class UICardRippleLayout extends MaterialRippleLayout {
    public UICardRippleLayout(Context context) {
        super(context);
    }

    public UICardRippleLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UICardRippleLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    public void setUICardRippleRoundedCorners(int rippleCornerRadius){
        setRippleRoundedCorners(true, rippleCornerRadius);
    }
}

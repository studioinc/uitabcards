package com.medstudioinc.uicard.cards;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.ViewGroup;

import com.medstudioinc.uicard.listener.OnCardViewClickListener;
import com.medstudioinc.uicard.listener.OnCardViewTouchListener;
import com.medstudioinc.uicard.listener.OnLongCardViewClickListener;
import com.medstudioinc.uicard.utils.CornerType;
import com.medstudioinc.uicard.utils.UICardAnimation;
import com.medstudioinc.uicard.widget.UICardBottomSectionBar;
import com.medstudioinc.uicard.widget.UICardGradientView;
import com.medstudioinc.uicard.widget.UICardImageView;
import com.medstudioinc.uicard.widget.UICardRippleLayout;
import com.medstudioinc.uicard.widget.UICardRootLayout;
import com.medstudioinc.uicard.widget.UICardRoundedLayout;
import com.medstudioinc.uicard.widget.UICardTextView;
import com.medstudioinc.uitabslib.R;

public class UICardViews extends UICardRootLayout {
    UICardTextView tvUICardTitle;
    UICardImageView ivUICardThumbnail;
    UICardRoundedLayout uiCardRoundedLayout;
    UICardRippleLayout uiCardRippleLayout;
    UICardGradientView uiCardGradientView;
    UICardRootLayout uiCardRootLayout;
    UICardBottomSectionBar uiCardBottomSectionBar;

    boolean isBottomSection;


    int width;
    int height;
    int margins;

    public OnLongCardViewClickListener longCardViewClickListener;
    public OnCardViewClickListener cardViewClickListener;
    public OnCardViewTouchListener cardViewTouchListener;

    public int getUICardMargins() {
        return margins;
    }
    public float getUICardWidth() {
        return width;
    }
    public float getUICardHeight() {
       return height;
    }

    public UICardViews(Context context, int width, int height,  int margins) {
        super(context);
        this.width = width;
        this.height = height;
        this.margins = margins;
    }

    public UICardViews(Context context) {
        super(context);
    }

    public UICardViews(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public UICardViews(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);


        uiCardRootLayout = (UICardRootLayout) this;
        ConstraintLayout.LayoutParams viewParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        viewParams.topMargin = getUICardMargins();
        viewParams.leftMargin = getUICardMargins();
        viewParams.bottomMargin = getUICardMargins();
        viewParams.rightMargin = getUICardMargins();
            /*viewParams.width = (int) getUICardWidth();
            viewParams.height = (int) getUICardHeight();*/
        uiCardRootLayout.setLayoutParams(viewParams);


        uiCardRippleLayout = new UICardRippleLayout(getContext());
        uiCardRippleLayout.setId(R.id.card_ripple_layout);
        ConstraintLayout.LayoutParams rippleParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        uiCardRippleLayout.setLayoutParams(rippleParams);

            /*uiCardRippleLayout.setPadding(1, 1, 1, 1);
            DisplayMetrics metrics = itemView.getContext().getResources().getDisplayMetrics();
            float cornerRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, 10, metrics);
            GradientDrawable gd = new GradientDrawable();
            //gd.setColor(Color.parseColor("#CCCCCC"));
            //gd.setColor(Color.argb(80, 200, 200, 50));
            gd.setColor(Color.parseColor(Utils.setColorAlpha(80, "#CCCCCC")));
            gd.setCornerRadius(cornerRadius);
            uiCardRippleLayout.setBackgroundDrawable(gd);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                uiCardRippleLayout.setElevation(8f);
            }*/
        //uiCardRootLayout.setBackgroundColor(Color.parseColor("#CCCCCC"));


        uiCardRoundedLayout = new UICardRoundedLayout(getContext());
        uiCardRoundedLayout.setId(R.id.rounded_corner_view);
        UICardRoundedLayout.LayoutParams roundedCornerParams = new UICardRoundedLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        uiCardRoundedLayout.setLayoutParams(roundedCornerParams);
        uiCardRoundedLayout.setBackgroundColor(Color.DKGRAY);


        ivUICardThumbnail = new UICardImageView(getContext());
        ivUICardThumbnail.setId(R.id.card_thumbnail);
        ivUICardThumbnail.setUICardThumbnailLayoutParams(getLayoutParams());


        uiCardBottomSectionBar = new UICardBottomSectionBar(getContext());
        uiCardBottomSectionBar.setId(R.id.section_view);
        ConstraintLayout.LayoutParams sectionParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
        sectionParams.matchConstraintDefaultHeight = ConstraintLayout.LayoutParams.MATCH_CONSTRAINT_WRAP;
        sectionParams.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID;
        sectionParams.height = 6;
        uiCardBottomSectionBar.setUICardSectionViewLayoutParams(sectionParams);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            uiCardBottomSectionBar.setElevation(15.0f);
        }

        uiCardGradientView = new UICardGradientView(getContext());
        uiCardGradientView.setUICardGradientViewLayoutParams(getLayoutParams());

        tvUICardTitle = new UICardTextView(getContext());
        ConstraintLayout.LayoutParams txtParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        txtParams.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
        txtParams.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID;
        txtParams.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID;
        txtParams.rightToRight = ConstraintLayout.LayoutParams.PARENT_ID;
        tvUICardTitle.setUICardTextLayoutParams(txtParams);
        tvUICardTitle.setUICardTextId(R.id.card_title);


        defaultValues();

        uiCardRootLayout.addView(uiCardRippleLayout);
        uiCardRippleLayout.addView(uiCardRoundedLayout);

        uiCardRippleLayout.setOnClickListener(v-> {
            if (cardViewClickListener != null) {
                UICardAnimation.setup(uiCardRootLayout).commit(new UICardAnimation.UICardAnimationListener() {
                    @Override
                    public void onAnimationEnd() {
                        cardViewClickListener.onClick(v);
                    }
                });
            }
        });
        uiCardRippleLayout.setOnLongClickListener(v-> {
            if (longCardViewClickListener != null){
                UICardAnimation.setup(uiCardRootLayout).commit(new UICardAnimation.UICardAnimationListener() {
                    @Override
                    public void onAnimationEnd() {
                        longCardViewClickListener.onLongClick(v);
                    }
                });
            }
            return false;
        });
        uiCardRippleLayout.setOnTouchListener((v, e)->{
            if (cardViewTouchListener != null){
                cardViewTouchListener.onTouch(v, e);
            }
            return false;
        });

        uiCardRoundedLayout.addView(ivUICardThumbnail);
        uiCardRoundedLayout.addView(uiCardGradientView);
        if (isBottomSection){
            uiCardRoundedLayout.addView(uiCardBottomSectionBar);
        }

        uiCardRoundedLayout.addView(tvUICardTitle);

    }

    private void defaultValues(){
        uiCardRippleLayout.setRippleDelayClick(true);
        uiCardRippleLayout.setRippleOverlay(true);
        uiCardRippleLayout.setRippleHover(true);
        uiCardRippleLayout.setRippleAlpha(1);
        uiCardRippleLayout.setRippleColor(Color.parseColor("#E9EBEE"));

        tvUICardTitle.setUICardTextShadowLayer(5, -2, -2, Color.DKGRAY);
        tvUICardTitle.setUICardTextColor(Color.WHITE);
        tvUICardTitle.setUICardTextFont("fonts/future.ttf");
        tvUICardTitle.setUICardTextSize(13);
    }

    public void setUICardRoundedCornerRadius(int radius, CornerType cornerType){
        uiCardRippleLayout.setUICardRippleRoundedCorners(radius);
        uiCardRoundedLayout.setUICardRoundedCornerRadius(radius, cornerType);
    }

    private ConstraintLayout.LayoutParams getRootLayoutParams(){
        ConstraintLayout.LayoutParams layoutGradientViewParams;
        if (isBottomSection){
            layoutGradientViewParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
            layoutGradientViewParams.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
            layoutGradientViewParams.bottomToTop = R.id.section_view;
            layoutGradientViewParams.height = (int) getUICardHeight();
            layoutGradientViewParams.matchConstraintDefaultHeight = ConstraintLayout.LayoutParams.MATCH_CONSTRAINT_SPREAD;
        } else {
            layoutGradientViewParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            layoutGradientViewParams.height = (int) getUICardHeight();
        }
        return layoutGradientViewParams;
    }

}

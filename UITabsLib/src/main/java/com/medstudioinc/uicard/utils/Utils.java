package com.medstudioinc.uicard.utils;

public class Utils {

    /**
     * @param colorCode color, without alpha
     * @param percentage         from 0.0 to 1.0
     * @return
     */
    public static String setColorAlpha(int percentage, String colorCode){
        double decValue = ((double)percentage / 100) * 255;
        String rawHexColor = colorCode.replace("#","");
        StringBuilder str = new StringBuilder(rawHexColor);

        if(Integer.toHexString((int)decValue).length() == 1)
            str.insert(0, "#0" + Integer.toHexString((int)decValue));
        else
            str.insert(0, "#" + Integer.toHexString((int)decValue));
        return str.toString();
    }

    /**
     * @param originalColor color, without alpha
     * @param alpha         from 0.0 to 1.0
     * @return
     */
    public static String addAlpha(String originalColor, double alpha) {
        long alphaFixed = Math.round(alpha * 255);
        String alphaHex = Long.toHexString(alphaFixed);
        if (alphaHex.length() == 1) {
            alphaHex = "0" + alphaHex;
        }
        originalColor = originalColor.replace("#", "#" + alphaHex);


        return originalColor;
    }
}

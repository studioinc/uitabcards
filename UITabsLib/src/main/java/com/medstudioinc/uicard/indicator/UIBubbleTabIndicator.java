package com.medstudioinc.uicard.indicator;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.ColorRes;
import android.support.annotation.FloatRange;
import android.support.annotation.StringRes;
import android.support.constraint.ConstraintLayout;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.medstudioinc.uicard.listener.OnCardViewClickListener;
import com.medstudioinc.uicard.listener.OnCardViewTouchListener;
import com.medstudioinc.uicard.listener.OnLongCardViewClickListener;
import com.medstudioinc.uicard.listener.UIBubbleTabIndicatorListener;
import com.medstudioinc.uicard.utils.GradientColors;
import com.medstudioinc.uicard.utils.ResizeWidthAnimation;
import com.medstudioinc.uicard.utils.UICardAnimation;
import com.medstudioinc.uicard.widget.UICardContentLayout;
import com.medstudioinc.uicard.widget.UICardContentLinearLayout;
import com.medstudioinc.uicard.widget.UICardRippleLayout;
import com.medstudioinc.uicard.widget.UICardRootLayout;
import com.medstudioinc.uicard.widget.UICardSelectorView;
import com.medstudioinc.uicard.widget.UICardSeparatorView;
import com.medstudioinc.uicard.widget.UICardTextView;
import com.medstudioinc.uitabslib.R;

import java.util.Locale;


@SuppressLint({ "InflateParams", "ClickableViewAccessibility" })
/**
 * A simple Tab with Material Design style
 * @author neokree
 *
 */ 
public class UIBubbleTabIndicator implements OnCardViewTouchListener {

    private final static int REVEAL_DURATION = 200;
    private final static int HIDE_DURATION = 300;

	private View completeView;

	private Resources res;
	private UIBubbleTabIndicatorListener listener;
	private GradientColors gradientColors;
	private int backgroundSolidColor;

	private int textColor;
	private int primaryColor;
	private int accentColor;

	private boolean active;
	private int position;

    private float density;
    private Point lastTouchedPoint;
    private float alpha;

    private int cardRoundedCornersRadius;

    private UICardTextView tvUICardTitle;
    private UICardRippleLayout uiCardRippleLayout;
    private UICardContentLayout uiCardContentLayout;
    //private UICardGradientView uiCardGradientView;
    private UICardRootLayout uiCardRootLayout;
    private UICardContentLinearLayout uiCardContentLinearLayout;
    private UICardSeparatorView uiCardSeparatorView;
    private UICardSelectorView uiCardSelector;

    private OnLongCardViewClickListener longCardViewClickListener;
    private OnCardViewClickListener cardViewClickListener;
    private OnCardViewTouchListener cardViewTouchListener;

    private int uiTabCardMargins;
    private int uiTabCarTopMargin;
    private int uiTabCarLeftMargin;
    private int uiTabCarRightMargin;
    private int uiTabCarBottomMargin;
    private float uiTabCardWidth;
    private float uiTabCardHeight;

    private GradientDrawable uiTabsCardGradientDrawable;

    ColorStateList[] colorStateList = new ColorStateList[3];

	public UIBubbleTabIndicator(Context ctx) {
	    init(ctx);
	}

    private void init(Context ctx){
        density = ctx.getResources().getDisplayMetrics().density;
        res = ctx.getResources();

        completeView = new UICardRootLayout(ctx);

        uiCardRootLayout = (UICardRootLayout) completeView;
        ConstraintLayout.LayoutParams viewParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        uiCardRootLayout.setLayoutParams(viewParams);

        uiCardRippleLayout = new UICardRippleLayout(ctx);
        uiCardRippleLayout.setId(R.id.card_ripple_layout);
        uiCardRippleLayout.post(()->{
            RelativeLayout.LayoutParams rippleParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            rippleParams.topMargin = uiTabCarTopMargin;
            rippleParams.leftMargin = uiTabCarLeftMargin;
            rippleParams.rightMargin = uiTabCarRightMargin;
            rippleParams.bottomMargin = uiTabCarBottomMargin;
            uiCardRippleLayout.setLayoutParams(rippleParams);
        });


        uiCardContentLayout = new UICardContentLayout(ctx);
        UICardContentLayout.LayoutParams countentLayoutParams = new UICardContentLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        uiCardContentLayout.setUICardContentLayoutParams(countentLayoutParams);


        uiCardContentLinearLayout = new UICardContentLinearLayout(ctx);
        uiCardContentLinearLayout.setOrientation(LinearLayout.VERTICAL);
        uiCardContentLinearLayout.setGravity(Gravity.CENTER);
        ConstraintLayout.LayoutParams linearParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        linearParams.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
        linearParams.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID;
        linearParams.rightToRight = ConstraintLayout.LayoutParams.PARENT_ID;
        linearParams.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID;
        uiCardContentLinearLayout.setLayoutParams(linearParams);

        tvUICardTitle = new UICardTextView(ctx);
        ConstraintLayout.LayoutParams txtParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tvUICardTitle.setLayoutParams(txtParams);
        tvUICardTitle.setId(R.id.card_title);


        uiCardSeparatorView = new UICardSeparatorView(ctx);
        ConstraintLayout.LayoutParams setparatorParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 5);
        uiCardSeparatorView.setLayoutParams(setparatorParams);

        uiCardSelector = new UICardSelectorView(ctx);
        LinearLayout.LayoutParams setlectorParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 3);
        uiCardSelector.setLayoutParams(setlectorParams);

       /* uiCardGradientView = new UICardGradientView(ctx);
        ConstraintLayout.LayoutParams uiCardGradientViewParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        uiCardGradientView.setUICardGradientViewLayoutParams(uiCardGradientViewParams);*/


        uiCardRootLayout.addView(uiCardRippleLayout);
        uiCardRippleLayout.addView(uiCardContentLayout);

        //uiCardContentLayout.addView(uiCardGradientView);
        uiCardContentLayout.addView(uiCardContentLinearLayout);

        uiCardContentLinearLayout.addView(tvUICardTitle);
        uiCardContentLinearLayout.addView(uiCardSeparatorView);
        uiCardContentLinearLayout.addView(uiCardSelector);



        // set the listener
        uiCardRippleLayout.setOnTouchListener((v, e)-> {
                if (cardViewTouchListener != null){
                    cardViewTouchListener.onTouch(v, e);
                }
            return false;
        });
        setOnTabCardViewTouchListener(this::onTouch);

        active = false;
        textColor = Color.WHITE; // default white text
        tvUICardTitle.setTypeface(null, Typeface.BOLD);
        defaultValues();
    }

    private void defaultValues(){
        uiCardRippleLayout.setRippleDelayClick(true);
        uiCardRippleLayout.setRippleOverlay(true);
        uiCardRippleLayout.setRippleHover(true);
        uiCardRippleLayout.setRippleAlpha(1);
        uiCardRippleLayout.setRippleColor(Color.parseColor("#E9EBEE"));

        //tvUICardTitle.setUICardTextShadowLayer(5, -2, -2, Color.DKGRAY);
        tvUICardTitle.setUICardTextColor(Color.WHITE);
        tvUICardTitle.setUICardTextSize(10);
    }

    public void setUITabCardPaddings(int left, int top, int right, int bottom){
        completeView.setPadding(left, top, right, bottom);
    }

    public void setUiTabCardMargins(int left, int top, int right, int bottom){
        this.uiTabCarLeftMargin = left;
	    this.uiTabCarTopMargin = top;
	    this.uiTabCarRightMargin = right;
	    this.uiTabCarBottomMargin = bottom;
    }

    public UIBubbleTabIndicator setUITabCardBackgroundColor(@FloatRange(from = 0, to = 1.0) float alpha, String color){
        this.backgroundSolidColor = Color.parseColor(color);
        this.alpha = alpha;
        if (uiCardContentLayout != null){
            getView().post(()->{
                uiCardContentLayout.setUICardRoundedCornerRadius(cardRoundedCornersRadius);
                if (getUITabCardPosition() == 0){
                    uiCardContentLayout.setUICardBackgroundColor(Color.BLACK);
                } else {
                    uiCardContentLayout.setUICardBackgroundColor(this.backgroundSolidColor);
                }
                uiCardContentLayout.setAlpha(this.alpha);
            });
        }
        return this;
    }

    public UIBubbleTabIndicator setUITabCardBackgroundColor(@FloatRange(from = 0, to = 1.0) float alpha, int color){
	    this.backgroundSolidColor = color;
        this.alpha = alpha;
        if (uiCardContentLayout != null){
            getView().post(()->{
                uiCardContentLayout.setUICardRoundedCornerRadius(cardRoundedCornersRadius);
                if (getUITabCardPosition() == 0){
                    uiCardContentLayout.setUICardBackgroundColor(Color.BLACK);
                } else {
                    uiCardContentLayout.setUICardBackgroundColor(this.backgroundSolidColor);
                }
                uiCardContentLayout.setAlpha(this.alpha);
            });
        }
        return this;
    }

    public UIBubbleTabIndicator setUITabCardBackgroundGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, String[] gradientColorRes){
        this.alpha = alpha;
        colorStateList[0] = ColorStateList.valueOf(Color.parseColor(checkHexColor(gradientColorRes[0])));
        colorStateList[1] = ColorStateList.valueOf(Color.parseColor(checkHexColor(gradientColorRes[1])));
        if (gradientColorRes.length > 2){
            colorStateList[2] = ColorStateList.valueOf(Color.parseColor(checkHexColor(gradientColorRes[2])));
        }
        if (uiCardContentLayout != null){
            if (gradientColorRes.length == 2){
                uiTabsCardGradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[] {
                        colorStateList[0].getDefaultColor(),
                        colorStateList[1].getDefaultColor()
                });
            } else if(gradientColorRes.length == 3){
                uiTabsCardGradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[] {
                        colorStateList[0].getDefaultColor(),
                        colorStateList[1].getDefaultColor(),
                        colorStateList[2].getDefaultColor()
                });
            }
            getView().post(()->{
                uiTabsCardGradientDrawable.setCornerRadius(this.cardRoundedCornersRadius);
                if (getUITabCardPosition() == 0){
                    uiCardContentLayout.setUICardBackgroundColor(Color.BLACK);
                } else {
                    uiCardContentLayout.setBackgroundDrawable(uiTabsCardGradientDrawable);
                }
                uiCardContentLayout.setAlpha(this.alpha);
            });
        }
        return this;
    }

    public UIBubbleTabIndicator setUITabCardBackgroundGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, @ColorRes int[] gradientColorRes){
        this.alpha = alpha;
        colorStateList[0] = ColorStateList.valueOf(res.getColor(gradientColorRes[0]));
        colorStateList[1] = ColorStateList.valueOf(res.getColor(gradientColorRes[1]));
        if (gradientColorRes.length > 2){
            colorStateList[3] = ColorStateList.valueOf(res.getColor(gradientColorRes[2]));
        }

        if (uiCardContentLayout != null){
            if (gradientColorRes.length == 2){
                uiTabsCardGradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[] {
                        colorStateList[0].getDefaultColor(),
                        colorStateList[1].getDefaultColor()
                });
            } else if(gradientColorRes.length == 3){
                uiTabsCardGradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[] {
                        colorStateList[0].getDefaultColor(),
                        colorStateList[1].getDefaultColor(),
                        colorStateList[2].getDefaultColor()
                });
            }
            getView().post(()->{
                uiTabsCardGradientDrawable.setCornerRadius(this.cardRoundedCornersRadius);
                if (getUITabCardPosition() == 0){
                    uiCardContentLayout.setUICardBackgroundColor(Color.BLACK);
                } else {
                    uiCardContentLayout.setBackgroundDrawable(uiTabsCardGradientDrawable);
                }
                uiCardContentLayout.setAlpha(this.alpha);
            });
        }
        return this;
    }

    /**
     * @deprecated use {@link #@setBackground(Drawable)} instead
     */
    public UIBubbleTabIndicator setUITabCardBackgroundGradientColors(GradientColors gradientColorRes){
        this.alpha = gradientColorRes.alpha();
        this.gradientColors = gradientColorRes;

        colorStateList[0] = ColorStateList.valueOf(Color.parseColor(this.gradientColors.startColor()));
        colorStateList[1] = ColorStateList.valueOf(Color.parseColor(this.gradientColors.endColor()));
        if (gradientColorRes.length() == 3){
            colorStateList[2] = ColorStateList.valueOf(Color.parseColor(this.gradientColors.centerColor()));
        }

        if (uiCardContentLayout != null){
            if (this.gradientColors.length() == 2){
                uiTabsCardGradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[] {
                        colorStateList[0].getDefaultColor(),
                        colorStateList[1].getDefaultColor()
                });
            } else if(this.gradientColors.length() == 3){
                uiTabsCardGradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[] {
                        colorStateList[0].getDefaultColor(),
                        colorStateList[1].getDefaultColor(),
                        colorStateList[2].getDefaultColor()
                });
            }

            getView().post(()->{
                uiTabsCardGradientDrawable.setCornerRadius(this.cardRoundedCornersRadius);
                if (getUITabCardPosition() == 0){
                    uiCardContentLayout.setUICardBackgroundColor(Color.BLACK);
                } else {
                    uiCardContentLayout.setBackgroundDrawable(uiTabsCardGradientDrawable);
                }
                uiCardContentLayout.setAlpha(this.alpha);
            });
        }
        return this;
    }

    public UIBubbleTabIndicator setUITabCardRoundedCornersRadius(int roundedCorners){
        this.cardRoundedCornersRadius = roundedCorners;

        if (uiCardContentLayout != null && uiCardRippleLayout != null) {
            if (roundedCorners > 0)
                uiCardRippleLayout.setUICardRippleRoundedCorners(cardRoundedCornersRadius);
                uiCardContentLayout.setUICardRoundedCornerRadius(cardRoundedCornersRadius);
        }
        return this;
    }

    private String checkHexColor(String color){
        String oldColor;
        String newColor = null;
        if (color != null){
            oldColor = color.replaceFirst("#", "");
            newColor = "#" + oldColor;
        }
        return newColor;
    }

	public void setUITabCardSelectorColor(int color) {
		this.accentColor = color;
        this.textColor = color;
	}
	
	public void setUITabCardBackgroundColor(int color) {
		this.primaryColor = color;
	}

    public void setUITabCardText(@StringRes int strRes){
        if (this.tvUICardTitle != null)
            this.tvUICardTitle.setText(res.getString(strRes));
    }

	public UIBubbleTabIndicator setUITabCardText(CharSequence text) {
        if (tvUICardTitle != null){
            tvUICardTitle.setUICardText(text.toString().toUpperCase(Locale.US));
        }
        return this;
	}

    public void setUITabCardTextColor(int color) {
        textColor = color;
        if(tvUICardTitle != null) {
            tvUICardTitle.setUICardTextColor(this.textColor);
        }
    }

    public void setUITabCardTextColor(ColorStateList colorRes){
        this.textColor = colorRes.getDefaultColor();
        if (this.tvUICardTitle != null)
            this.tvUICardTitle.setTextColor(colorRes);
    }

    public void setUITabCardTextColor(String colorRes){
        this.textColor = Color.parseColor(colorRes);
        if (this.tvUICardTitle != null)
            this.tvUICardTitle.setTextColor(this.textColor);
    }

    public void setUITabCardTextSize(float textSize){
        if (this.tvUICardTitle != null)
            this.tvUICardTitle.setTextSize(textSize);
    }

    public void setUITabCardTextTypeface(Typeface typeface){
        if (this.tvUICardTitle != null)
            this.tvUICardTitle.setTypeface(typeface);
    }

    public void setUITabCardTextTypeface(String typefacePath){
        Typeface cardTitleTypeface = Typeface.createFromAsset(res.getAssets(), typefacePath);
        if (this.tvUICardTitle != null)
            this.tvUICardTitle.setTypeface(cardTitleTypeface);
    }

    public void setUITabCardTextTypeface(Typeface typeface, int style){
        if (this.tvUICardTitle != null)
            this.tvUICardTitle.setTypeface(typeface, style);
    }

    public void setUITabCardTextTypeface(String typefacePath, int style){
        Typeface cardTitleTypeface = Typeface.createFromAsset(res.getAssets(), typefacePath);
        if (this.tvUICardTitle != null)
            this.tvUICardTitle.setTypeface(cardTitleTypeface, style);
    }

    public void setUITabCardTextShadow(float radius, float dx, float dy, int color){
        if (this.tvUICardTitle != null)
            this.tvUICardTitle.setShadowLayer(radius, dx, dy, color);
    }

    public void setTabCardRippleDelayClick(boolean delayClick){
        if (this.uiCardRippleLayout != null){
            this.uiCardRippleLayout.setRippleDelayClick(delayClick);
        }
    }

    public void setTabCardRippleAlpha(Integer rippleAlpha){
        if (this.uiCardRippleLayout != null){
            this.uiCardRippleLayout.setRippleAlpha(rippleAlpha);
        }
    }

    public void setTabCardRippleBackground(int rippleBackground){
        if (this.uiCardRippleLayout != null){
            this.uiCardRippleLayout.setRippleBackground(rippleBackground);
        }
    }

    public void setTabCardRippleOverlay(boolean rippleOverlay){
        if (this.uiCardRippleLayout != null){
            this.uiCardRippleLayout.setRippleOverlay(rippleOverlay);
        }
    }

    public void setTabCardRippleHover(boolean rippleHover){
        if (this.uiCardRippleLayout != null){
            this.uiCardRippleLayout.setRippleHover(rippleHover);
        }
    }

    public void setTabCardRippleColor(int rippleColor){
        if (this.uiCardRippleLayout != null){
            this.uiCardRippleLayout.setRippleColor(rippleColor);
        }
    }

    public void setTabCardDefaultRippleAlpha(int rippleColor){
        if (this.uiCardRippleLayout != null){
            this.uiCardRippleLayout.setDefaultRippleAlpha(rippleColor);
        }
    }

    public void setTabCardRippleDuration(int rippleColor){
        if (this.uiCardRippleLayout != null){
            this.uiCardRippleLayout.setRippleDuration(rippleColor);
        }
    }

    public void setTabCardRippleRadius(float rippleColor){
        if (this.uiCardRippleLayout != null){
            this.uiCardRippleLayout.setRadius(rippleColor);
        }
    }

    public void setTabCardRippleDiameter(int rippleColor){
        if (this.uiCardRippleLayout != null){
            this.uiCardRippleLayout.setRippleDiameter(rippleColor);
        }
    }

    public void setTabCardRippleFadeDuration(int rippleColor){
        if (this.uiCardRippleLayout != null){
            this.uiCardRippleLayout.setRippleFadeDuration(rippleColor);
        }
    }

    public void setTabCardRippleInAdapter(boolean rippleColor){
        if (this.uiCardRippleLayout != null){
            this.uiCardRippleLayout.setRippleInAdapter(rippleColor);
        }
    }

    public void setTabCardRipplePersistent(boolean rippleColor){
        if (this.uiCardRippleLayout != null){
            this.uiCardRippleLayout.setRipplePersistent(rippleColor);
        }
    }

    public void disableUITabCard() {
        // set 60% alpha to text color
        if(tvUICardTitle != null)
            if (gradientColors != null){
                uiCardContentLayout.setUICardRoundedCornerRadius(cardRoundedCornersRadius);
                //uiCardContentLayout.setUICardGradientColors(0.78f, gradientColors.startColor(), gradientColors.endColor());
                uiCardContentLayout.setUICardGradientColors(alpha, gradientColors.startColor(), gradientColors.endColor());
            } else if (colorStateList.length >= 2 && colorStateList.length < 3){
                //uiCardContentLayout.setUICardGradientColors(0.78f, new ColorStateList[]{colorStateList[0], colorStateList[1]});
                uiCardContentLayout.setUICardGradientColors(alpha, new ColorStateList[]{colorStateList[0], colorStateList[1]});
            } else {
                uiCardContentLayout.setUICardBackgroundColor(this.backgroundSolidColor);
                //Toast.makeText(getView().getContext(), "", Toast.LENGTH_SHORT).show();
            }
            //tvUICardTitle.setTextColor(Color.argb(0x90 ,Color.red(this.textColor), Color.green(this.textColor), Color.blue(this.textColor)));



        setTabSelectedAnimate(uiCardSelector, false);

        uiCardSelector.setCardSelectorBackgroundColor(Color.TRANSPARENT);

        active = false;

        if(listener != null)
            listener.onTabUnselected(this);
    }

    public void activateUITabCard() {
        // set full color text
        if(tvUICardTitle != null)
            uiCardContentLayout.setUICardBackgroundColor(Color.BLACK);
           /* if (gradientColors != null){
                uiCardContentLayout.setUICardRoundedCornerRadius(true, cardRoundedCornersRadius);
                uiCardContentLayout.setUICardGradientColors(gradientColors.alpha(), gradientColors.startColor(), gradientColors.endColor());
            } else if (colorStateList.length >= 2 && colorStateList.length < 3){
                uiCardContentLayout.setUICardGradientColors(alpha, new ColorStateList[]{colorStateList[0], colorStateList[1]});
            }*/
         //tvUICardTitle.setUICardTextColor(textColor);

        setTabSelectedAnimate(uiCardSelector, true);

        setSelectorState(uiCardSelector);
        this.active = true;
    }

    private void setSelectorState(View view){
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(this.accentColor);
        drawable.setCornerRadius(30);
        view.setBackgroundDrawable(drawable);
        //drawable.setStroke(3, Color.RED); // set stroke width and stroke color
    }

    private void setTabSelectedAnimate(View view, boolean active){
        if (active){
            ResizeWidthAnimation anim = new ResizeWidthAnimation(view, getUITabCardTextLenght());
            anim.setDuration(350);
            uiCardSelector.startAnimation(anim);
        }
        UICardAnimation.setup(view).commit(new UICardAnimation.UICardAnimationListener(){
            @Override
            public void onAnimationStart() {
                super.onAnimationStart();
            }

            @Override
            public void onAnimationEnd() {
                super.onAnimationEnd();
            }
        });
    }
	
	public boolean isSelected() {
		return active;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
        lastTouchedPoint = new Point();
		lastTouchedPoint.x = (int) event.getX();
		lastTouchedPoint.y = (int) event.getY();

        // new effects
        if(event.getAction() == MotionEvent.ACTION_UP) {

            // set the click
            if(listener != null) {

                if(active) {
                    // if the tab is active when the user click on it it will be reselect
                    listener.onTabReselected(this);
                }
                else {
                    listener.onTabSelected(this);
                }
            }
            // if the tab is not activated, it will be active
            if(!active)
                this.activateUITabCard();

            return true;
        }

		return false;
	}
	
	public View getView() {
		return completeView;
	}
	
	public UIBubbleTabIndicator setUITabCardListener(UIBubbleTabIndicatorListener listener) {
		this.listener = listener;
		return this;
	}

    public UIBubbleTabIndicatorListener getTabCardListener() {
        return listener;
    }

	public int getUITabCardPosition() {
		return position;
	}

	public void setUITabCardPosition(int position) {
		this.position = position;
	}

    private int getUITabCardTextLenght() {
       String textString = tvUICardTitle.getText().toString();
        Rect bounds = new Rect();
        Paint textPaint = tvUICardTitle.getPaint();
        textPaint.getTextBounds(textString,0,textString.length(),bounds);
        return bounds.width();
   }

    public UIBubbleTabIndicator setUITabCardWidth(float width){
        this.uiTabCardWidth = width;
        return this;
    }

    public UIBubbleTabIndicator setUITabCardHeight(float height){
        this.uiTabCardHeight = height;
        return this;
    }

    public UIBubbleTabIndicator setUITabCardWidthAndHeight(float width, float height){
        this.uiTabCardWidth = width;
        this.uiTabCardHeight = height;
        return this;
    }

    public float getUITabCardMinHeight() {
        if (uiTabCardWidth == 0){
            return getUITabCardTextLenght();
        } else {
            return uiTabCardHeight;
        }
    }

    public float getUITabCardMinWidth() {
        if (uiTabCardWidth == 0){
            return getUITabCardTextLenght();
        } else {
            return uiTabCardWidth;
        }
   }

    public int getUITabCardMargins(){
        return uiTabCardMargins;
    }

    public float getUITabCardWidth(){
        return uiTabCardWidth;
    }

    public float getUITabCardHeight(){
        return uiTabCardHeight;
    }

    /**
     * Register a callback to be invoked when this view is clicked. If this view is not
     * clickable, it becomes clickable.
     *
     * @param listener The callback that will run
     *
     * @see ##setClickable(boolean)
     */
    private void setOnTabCardViewClickListener(OnCardViewClickListener listener){
        this.cardViewClickListener = listener;
    }

    /**
     * Register a callback to be invoked when this view is clicked and held. If this view is not
     * long clickable, it becomes long clickable.
     *
     * @param listener The callback that will run
     *
     * @see ##setClickable(boolean)
     */
    private void setOnTabCardViewLongClickListener(OnLongCardViewClickListener listener){
        this.longCardViewClickListener = listener;
    }

    private void setOnTabCardViewTouchListener(OnCardViewTouchListener listener){
        this.cardViewTouchListener = listener;
    }
}

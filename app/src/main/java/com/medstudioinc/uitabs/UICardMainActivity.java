package com.medstudioinc.uitabs;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.medstudioinc.uicard.widget.UICardViewPager;

public class UICardMainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {
    private UICardPagerAdapter pagerAdapter;
    private UICardViewPager pager;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        RelativeLayout relativeLayout = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams
                = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT);
        relativeLayout.setLayoutParams(layoutParams);

        relativeLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));



        pager = new UICardViewPager(this);
        pager.setId(R.id.pager);
        RelativeLayout.LayoutParams pagerParams
                = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        pagerParams.addRule(RelativeLayout.BELOW, R.id.tabs);
        pager.setLayoutParams(pagerParams);


        relativeLayout.addView(pager);

        setContentView(relativeLayout, layoutParams);

        pagerAdapter = new UICardPagerAdapter(getSupportFragmentManager());
        pagerAdapter.add(new UICardFragment());
        pager.setOffscreenPageLimit(pagerAdapter.getCount());
        pager.setOnPageChangeListener(this);
        pager.setScrollDurationFactor(3);
        pager.setAdapter(pagerAdapter);

    }



    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menuItems(menu);
        return true;
    }

    private void menuItems(Menu menuItem){
        menuItem.add(0, 0xFFF001, Menu.NONE, "Tabs Cards").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        menuItem.add(0, 0xFFF002, Menu.NONE, "Tabs Indicator").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        menuItem.add(0, 0xFFF003, Menu.NONE, "Card View").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        menuItem.add(0, 0xFFF004, Menu.NONE, "Simple Tabs").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case 0xFFF001:
                startActivity(new Intent(this, TabsCardActivity.class));
                return true;
            case 0xFFF002:
                startActivity(new Intent(this, BubbleTabIndicatorActivity.class));
                return true;
            case 0xFFF003:
                startActivity(new Intent(this, CardViewActivity.class));
                return true;
            case 0xFFF004:
                startActivity(new Intent(this, SimpleTabsActivity.class));
                return true;
        }
        return false;
    }
}

package com.medstudioinc.uicard.tabs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.medstudioinc.uicard.listener.UITabsCardListener;
import com.medstudioinc.uicard.utils.CornerType;
import com.medstudioinc.uicard.utils.TextShadowLayer;
import com.medstudioinc.uitabslib.R;

import java.util.LinkedList;
import java.util.List;


/**
 * A Toolbar that contains multiple tabs
 * @author neokree
 *
 */
@SuppressLint("InflateParams")
public class UITabsCardHost extends RelativeLayout implements View.OnClickListener {
    private DisplayMetrics metrics;

	private int primaryColor;
	private int accentColor;
	private int textColor;
	public List<UITabsCard> tabs;
    private boolean isTablet;
    private float density;
    private boolean scrollable;

    private HorizontalScrollView scrollView;
    private LinearLayout layout;
    private ImageButton left;
    private ImageButton right;

    private static int tabSelected;

    private int tabHeight;
    private int tabWidth;

    private int cornerRadius;
    private float uiTabCardWidth;
    private float uiTabCardHeight;

    private float textSize;
    private Typeface textTypeface;
    private String typefacePath;
    private TextShadowLayer textShadowLayer;
    private CornerType cornerType;

    private boolean rippleHover;
    private boolean rippleOverlay;
    private boolean rippleDelayClick;
    private boolean ripplePersistent;
    private boolean rippleInAdapter;

    private float rippleRadius;
    private int rippleFadeDuration;
    private int rippleColor;
    private int rippleDiameter;
    private int rippleBackground;
    private int defaultRippleAlpha;
    private int rippleDuration;
    private Integer rippleAlpha;

    private int tabLeftPadding;
    private int tabTopPadding;
    private int tabRightPadding;
    private int tabBottomPadding;

    private int tabLeftMargin;
    private int tabTopMargin;
    private int tabRightMargin;
    private int tabBottomMargin;



    private UITabsCardListener listener;
    private RelativeLayout.LayoutParams layoutParams;

	public UITabsCardHost(Context context) {
		this(context, null);
	}

	public UITabsCardHost(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public UITabsCardHost(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		isInEditMode();

        scrollView = new HorizontalScrollView(context);
        scrollView.setOverScrollMode(HorizontalScrollView.OVER_SCROLL_NEVER);
        scrollView.setHorizontalScrollBarEnabled(false);
        layout = new LinearLayout(context);
        scrollView.addView(layout);

		// get attributes
		if(attrs != null) {
			TypedArray a = context.getTheme().obtainStyledAttributes(attrs,R.styleable.UITabHost, 0, defStyleAttr);

			try {
                // custom attributes
                primaryColor = a.getColor(R.styleable.UITabHost_primaryColor, Color.parseColor("#009688"));
                accentColor = a.getColor(R.styleable.UITabHost_accentColor,Color.parseColor("#00b0ff"));
                textColor = a.getColor(R.styleable.UITabHost_textColor,Color.WHITE);
			} finally {
				a.recycle();
			}
		}
        this.isInEditMode();
        scrollable = false;
        isTablet = this.getResources().getBoolean(R.bool.isTablet);
        density = this.getResources().getDisplayMetrics().density;
        metrics = getContext().getResources().getDisplayMetrics();
        layoutParams = new RelativeLayout.LayoutParams(HorizontalScrollView.LayoutParams.MATCH_PARENT, HorizontalScrollView.LayoutParams.MATCH_PARENT);
        tabSelected = 0;
        primaryColor = Color.TRANSPARENT;

		// initialize tabs list
		tabs = new LinkedList<>();

        // set background color
        super.setBackgroundColor(primaryColor);
	}


    public void setUITabCardWidthAndHeight(int tabWidth, int tabHeight){
        this.uiTabCardWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, tabWidth, metrics);
        this.uiTabCardHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, tabHeight, metrics);

        layoutParams.height = (int) this.uiTabCardHeight;
        setLayoutParams(layoutParams);
    }

    public void setUITabCornerRadius(int radius, CornerType cornerType) {
        this.cornerRadius = radius;
        this.cornerType = cornerType;
    }


    public void setUITabBackgroundColor(int color) {
		this.primaryColor = color;

        this.setBackgroundColor(primaryColor);

		for(UITabsCard tab : tabs) {
			tab.setUITabCardBackgroundColor(color);
		}
	}

	public void setUITabSelectorColor(int color) {
		this.accentColor = color;

		for(UITabsCard tab : tabs) {
			tab.setUITabCardSelectorColor(color);
		}
	}

	public void setUITabTextColor(int color) {
		this.textColor = color;

		for(UITabsCard tab : tabs) {
			tab.setUITabCardTextColor(color);
		}
	}

    public void setUITabCardTextColor(ColorStateList colorRes){
        this.textColor = colorRes.getDefaultColor();
        for(UITabsCard tab : tabs) {
            tab.setUITabCardTextColor(textColor);
        }
    }

    public void setUITabCardTextColor(String colorRes){
        this.textColor = Color.parseColor(colorRes);
        for(UITabsCard tab : tabs) {
            tab.setUITabCardTextColor(textColor);
        }
    }


    public void setUITabCardTextSize(float size){
	    this.textSize = size;
        for(UITabsCard tab : tabs) {
            tab.setUITabCardTextSize(textSize);
        }
    }

    public void setUITabCardTextFont(Typeface typeface){
	    this.textTypeface = typeface;
        for(UITabsCard tab : tabs) {
            tab.setUITabCardTextTypeface(textTypeface);
        }
    }

    public void setUITabCardTextFont(String typeface){
	    this.typefacePath = typeface;
        for(UITabsCard tab : tabs) {
            tab.setUITabCardTextTypeface(typefacePath);
        }
    }

    public void setUITabCardTextShadow(float radius, float dx, float dy, int color){
	    this.textShadowLayer = new TextShadowLayer(radius, dx, dy, color);
        for(UITabsCard tab : tabs) {
            tab.setUITabCardTextShadow(
                    this.textShadowLayer.getTextShadowRadius(),
                    this.textShadowLayer.getTextShadowDy(),
                    this.textShadowLayer.getTextShadowDx(),
                    this.textShadowLayer.getTextShadowColor()
            );
        }
    }

    public void setTabCardRippleDelayClick(boolean delayClick){
	    this.rippleDelayClick = delayClick;
        for(UITabsCard tab : tabs) {
            tab.setTabCardRippleDelayClick(delayClick);
        }
    }

    public void setTabCardRippleAlpha(Integer rippleAlpha){
        this.rippleAlpha = rippleAlpha;
        for(UITabsCard tab : tabs) {
            tab.setTabCardRippleAlpha(this.rippleAlpha);
        }
    }

    public void setTabCardRippleBackground(int rippleBackground){
        this.rippleBackground = rippleBackground;
        for(UITabsCard tab : tabs) {
            tab.setTabCardRippleBackground(this.rippleAlpha);
        }
    }

    public void setTabCardRippleOverlay(boolean rippleOverlay){
	    this.rippleOverlay = rippleOverlay;
        for(UITabsCard tab : tabs) {
            tab.setTabCardRippleBackground(this.rippleAlpha);
        }
    }

    public void setTabCardRippleHover(boolean rippleHover){
	    this.rippleHover = rippleHover;
        for(UITabsCard tab : tabs) {
            tab.setTabCardRippleBackground(this.rippleAlpha);
        }
    }

    public void setTabCardRippleColor(int rippleColor){
	    this.rippleColor = rippleColor;
        for(UITabsCard tab : tabs) {
            tab.setTabCardRippleBackground(this.rippleAlpha);
        }
    }

    public void setTabCardDefaultRippleAlpha(int defaultRippleAlpha){
	    this.defaultRippleAlpha = defaultRippleAlpha;
        for(UITabsCard tab : tabs) {
            tab.setTabCardRippleBackground(this.rippleAlpha);
        }
    }

    public void setTabCardRippleDuration(int rippleDuration){
	    this.rippleDuration = rippleDuration;
        for(UITabsCard tab : tabs) {
            tab.setTabCardRippleBackground(this.rippleAlpha);
        }
    }

    public void setTabCardRippleRadius(float rippleRadius){
	    this.rippleRadius = rippleRadius;
        for(UITabsCard tab : tabs) {
            tab.setTabCardRippleBackground(this.rippleAlpha);
        }
    }

    public void setTabCardRippleDiameter(int rippleDiameter){
	    this.rippleDiameter = rippleDiameter;
        for(UITabsCard tab : tabs) {
            tab.setTabCardRippleBackground(this.rippleAlpha);
        }
    }

    public void setTabCardRippleFadeDuration(int rippleFadeDuration){
	    this.rippleFadeDuration = rippleFadeDuration;
        for(UITabsCard tab : tabs) {
            tab.setTabCardRippleBackground(this.rippleAlpha);
        }
    }

    public void setTabCardRippleInAdapter(boolean rippleInAdapter){
	    this.rippleInAdapter = rippleInAdapter;
        for(UITabsCard tab : tabs) {
            tab.setTabCardRippleBackground(this.rippleAlpha);
        }
    }

    public void setTabCardRipplePersistent(boolean ripplePersistent){
	    this.ripplePersistent = ripplePersistent;
        for(UITabsCard tab : tabs) {
            tab.setTabCardRippleBackground(this.rippleAlpha);
        }
    }


    public void setUITabCardPaddings(int left, int top, int right, int bottom){
        this.tabLeftPadding = left;
        this.tabTopPadding = top;
        this.tabRightPadding = right;
        this.tabBottomPadding = bottom;

        for(UITabsCard tab : tabs) {
            tab.setUITabCardPaddings(tabLeftPadding, tabTopPadding, tabRightPadding, tabBottomPadding);
        }
    }

    public void setUITabCardMargins(int left, int top, int right, int bottom){
        this.tabLeftMargin = left;
        this.tabTopMargin = top;
        this.tabRightMargin = right;
        this.tabBottomMargin = bottom;

        for(UITabsCard tab : tabs) {
            tab.setUiTabCardMargins(tabLeftMargin, tabTopMargin, tabRightMargin, tabBottomMargin);
        }
    }


	public void addUITabCard(UITabsCard tab) {
        // add properties to tab
        tab.setUITabCardSelectorColor(accentColor);
        tab.setUITabCardBackgroundColor(primaryColor);
        tab.setUITabCardTextColor(textColor);
        tab.setUITabCardTextSize(textSize);
        tab.setUITabCardTextTypeface(textTypeface);
        tab.setUITabCardTextTypeface(typefacePath);
        tab.setUITabCardPosition(tabs.size());
        tab.setUITabCardRoundedCornersRadius(cornerRadius, cornerType);
        tab.setUITabCardWidthAndHeight(uiTabCardWidth, uiTabCardHeight);
        tab.setUITabCardPaddings(tabLeftPadding, tabTopPadding, tabRightPadding, tabBottomPadding);
        tab.setUiTabCardMargins(tabLeftMargin, tabTopMargin, tabRightMargin, tabBottomMargin);
        tab.setUITabCardListener(listener);

        // insert new tab in list
        tabs.add(tab);

        /*if(tabs.size() == 4 && !hasThumbnail) {
            // switch tabs to scrollable before its draw
            scrollable = true;
        }*/

        if(tabs.size() == 6) {
            scrollable = true;
        }
	}

	public UITabsCard newUITabCard() {
		return new UITabsCard(this.getContext());
	}

	public void setUITabSelectedNavigationItem(int position) {
		if(position < 0 || position > tabs.size()) {
			throw new RuntimeException("Index overflow");
		} else {
			// tab at position will select, other will deselect
			for(int i = 0; i < tabs.size(); i++) {
				UITabsCard tab = tabs.get(i);

				if(i == position) {
					tab.activateUITabCard();
				}
				else {
					tabs.get(i).disableUITabCard();
				}
			}

            // move the tab if it is slidable
            if(scrollable) {
                scrollTo(position);
            }

            tabSelected = position;
		}

	}

    private void scrollTo(int position) {
        int totalWidth = 0;//(int) ( 60 * density);
        for (int i = 0; i < position; i++) {
            int width = tabs.get(i).getView().getWidth();
            if(width == 0) {
                if(!isTablet)
                    width = (int) (tabs.get(i).getUITabCardMinWidth() + (24 * density));
                else
                    width = (int) (tabs.get(i).getUITabCardMinWidth() + (48 * density));
            }

            totalWidth += width;
        }
        scrollView.smoothScrollTo(totalWidth, 0);
    }

	@Override
	public void removeAllViews() {
		for(int i = 0; i<tabs.size();i++) {
			tabs.remove(i);
		}
		layout.removeAllViews();
        super.removeAllViews();
	}

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if(this.getWidth() != 0 && tabs.size() != 0)
            notifyDataSetChanged();
    }


    public void notifyDataSetChanged() {
        super.removeAllViews();
        layout.removeAllViews();


        if (!scrollable) { // not scrollable tabs
            int tabWidth = this.getWidth() / tabs.size();

            // set params for resizing tabs width
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(tabWidth, HorizontalScrollView.LayoutParams.MATCH_PARENT);
            for (UITabsCard t : tabs) {
                layout.addView(t.getView(), params);
            }

        } else { //scrollable tabs
            if(!isTablet) {
                for (int i = 0; i < tabs.size(); i++) {
                    LinearLayout.LayoutParams params;
                    UITabsCard tab = tabs.get(i);

                    int tabWidth = (int) (tab.getUITabCardMinWidth() + (24 * density)); // 12dp + text/icon width + 12dp
                    this.tabWidth = tabWidth;
                    this.tabHeight = (int) (tab.getUITabCardMinHeight() + (24 * density)); // 12dp + text/icon width + 12dp

                    if (i == 0) {
                        // first tab
                        View view = new View(layout.getContext());
                        view.setMinimumWidth((int) (2 * density)); // default (60)
                        layout.addView(view);
                    }

                    params = new LinearLayout.LayoutParams(this.tabWidth, this.tabHeight); //HorizontalScrollView.LayoutParams.MATCH_PARENT
                    params = new LinearLayout.LayoutParams(this.tabWidth, HorizontalScrollView.LayoutParams.MATCH_PARENT); //HorizontalScrollView.LayoutParams.MATCH_PARENT
                    //
                    //params.setMargins(-5, -5, -5, -5);
                    layout.addView(tab.getView(), params);

                    if (i == tabs.size() - 1) {
                        // last tab
                        View view = new View(layout.getContext());
                        view.setMinimumWidth((int) (2 * density)); // default (60)
                        layout.addView(view);
                    }
                }
            } else {
                // is a tablet
                for (int i = 0; i < tabs.size(); i++) {
                    LinearLayout.LayoutParams params;
                    UITabsCard tab = tabs.get(i);

                    int tabWidth = (int) (tab.getUITabCardMinWidth() + (48 * density)); // 24dp + text/icon width + 24dp
                    this.tabWidth = tabWidth;
                    this.tabHeight = (int) (tab.getUITabCardMinHeight() + (48 * density)); // 12dp + text/icon width + 12dp

                    //params = new LinearLayout.LayoutParams(this.tabWidth, this.tabHeight);
                    params = new LinearLayout.LayoutParams(this.tabWidth, HorizontalScrollView.LayoutParams.MATCH_PARENT);
                    layout.addView(tab.getView(), params);
                }
            }
        }

        if (isTablet && scrollable) {
            // if device is a tablet and have scrollable tabs add right and left arrows
            Resources res = getResources();

            left = new ImageButton(this.getContext());
            left.setId(R.id.left);
            left.setImageDrawable(res.getDrawable(R.drawable.left_arrow));
            left.setBackgroundColor(Color.TRANSPARENT);
            left.setOnClickListener(this);

            // set 56 dp width and 48 dp height
            LayoutParams paramsLeft = new LayoutParams((int)( 56 * density),(int) (48 * density));
            paramsLeft.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            paramsLeft.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            paramsLeft.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            this.addView(left,paramsLeft);

            right = new ImageButton(this.getContext());
            right.setId(R.id.right);
            right.setImageDrawable(res.getDrawable(R.drawable.right_arrow));
            right.setBackgroundColor(Color.TRANSPARENT);
            right.setOnClickListener(this);

            LayoutParams paramsRight = new LayoutParams((int)( 56 * density),(int) (48 * density));
            paramsRight.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            paramsRight.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            paramsRight.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            this.addView(right,paramsRight);

            LayoutParams paramsScroll = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            paramsScroll.addRule(RelativeLayout.LEFT_OF, R.id.right);
            paramsScroll.addRule(RelativeLayout.RIGHT_OF,R.id.left);
            this.addView(scrollView,paramsScroll);
        }
        else {
            // if is not a tablet add only scrollable content
            LayoutParams paramsScroll;
            paramsScroll = new LayoutParams(LayoutParams.MATCH_PARENT, HorizontalScrollView.LayoutParams.MATCH_PARENT);
            this.addView(scrollView,paramsScroll);
        }

        this.setUITabSelectedNavigationItem(tabSelected);
    }

    public UITabsCard getCurrentUITab() {
        for(UITabsCard tab : tabs) {
            if (tab.isSelected())
                return tab;
        }

        return null;
    }

    @Override
    public void onClick(View v) { // on tablet left/right button clicked
        int currentPosition = this.getCurrentUITab().getUITabCardPosition();

        if (v.getId() == R.id.right && currentPosition < tabs.size() -1) {
            currentPosition++;

            // set next tab selected
            this.setUITabSelectedNavigationItem(currentPosition);

            // change fragment
            tabs.get(currentPosition).getTabCardListener().onTabSelected(tabs.get(currentPosition));
            return;
        }

        if(v.getId() == R.id.left && currentPosition > 0) {
            currentPosition--;

            // set previous tab selected
            this.setUITabSelectedNavigationItem(currentPosition);
            // change fragment
            tabs.get(currentPosition).getTabCardListener().onTabSelected(tabs.get(currentPosition));
            return;
        }

    }

    public void setUITabListener(UITabsCardListener listener) {
        this.listener = listener;
    }
}

package com.medstudioinc.uicard.utils;

public class UICardLayoutParams {
    private int top;
    private int left;
    private int right;
    private int bottom;

    public UICardLayoutParams(int top, int left, int right, int bottom) {
        this.top = top;
        this.left = left;
        this.right = right;
        this.bottom = bottom;
    }

    public UICardLayoutParams(int margins) {
        this.top = margins;
        this.left = margins;
        this.right = margins;
        this.bottom = margins;
    }

    public int getTop() {
        return top;
    }

    public int getLeft() {
        return left;
    }

    public int getRight() {
        return right;
    }

    public int getBottom() {
        return bottom;
    }


}

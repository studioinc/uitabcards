package com.medstudioinc.uicard.utils;

import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;

import com.medstudioinc.uicard.listener.DrawingListener;

import org.jetbrains.annotations.NotNull;

import kotlin.jvm.functions.Function1;


public class CanvasRounder {
    private final Path path;
    private RectF rectF;
    private final CornersHolder cornersHolder;

    public final float getTopLeftCornerRadius() {
        return this.cornersHolder.getTopLeftCornerRadius();
    }

    public final void setTopLeftCornerRadius(float value) {
        this.cornersHolder.setTopLeftCornerRadius(value);
        this.resetPath();
    }

    public final float getTopRightCornerRadius() {
        return this.cornersHolder.getTopRightCornerRadius();
    }

    public final void setTopRightCornerRadius(float value) {
        this.cornersHolder.setTopRightCornerRadius(value);
        this.resetPath();
    }

    public final float getBottomRightCornerRadius() {
        return this.cornersHolder.getBottomRightCornerRadius();
    }

    public final void setBottomRightCornerRadius(float value) {
        this.cornersHolder.setBottomRightCornerRadius(value);
        this.resetPath();
    }

    public final float getBottomLeftCornerRadius() {
        return this.cornersHolder.getBottomLeftCornerRadius();
    }

    public final void setBottomLeftCornerRadius(float value) {
        this.cornersHolder.setBottomLeftCornerRadius(value);
        this.resetPath();
    }

    public final float getCornerRadius() {
        return this.getTopLeftCornerRadius() == this.getTopRightCornerRadius()
                && this.getTopRightCornerRadius() == this.getBottomRightCornerRadius() &&
                this.getBottomRightCornerRadius() == this.getBottomLeftCornerRadius() ?
                this.getTopLeftCornerRadius() : 0.0f / 0.0f;
    }

    public final void setCornerRadius(float value) {
        this.cornersHolder.setTopLeftCornerRadius(value);
        this.cornersHolder.setTopRightCornerRadius(value);
        this.cornersHolder.setBottomRightCornerRadius(value);
        this.cornersHolder.setBottomLeftCornerRadius(value);
        this.resetPath();
    }

    public final void round(@NotNull Canvas canvas, @NotNull Function1 drawFunction) {
        int save = canvas.save();
        canvas.clipPath(this.path);
        drawFunction.invoke(canvas);
        canvas.restoreToCount(save);
    }


    public final void round(@NotNull Canvas canvas, DrawingListener drawingListener) {
        int save = canvas.save();
        canvas.clipPath(this.path);
        if (drawingListener != null){
            drawingListener.onProcesses(canvas);
        }
        canvas.restoreToCount(save);
    }

    public final void updateSize(int currentWidth, int currentHeight) {
        this.rectF = new RectF(0.0F, 0.0F, (float)currentWidth, (float)currentHeight);
        this.resetPath();
    }

    private final void resetPath() {
        this.path.reset();
        Extensions.addRoundRectWithRoundCorners(this.path, this.rectF,
                this.getTopLeftCornerRadius(),
                this.getTopRightCornerRadius(),
                this.getBottomRightCornerRadius(),
                this.getBottomLeftCornerRadius());
        this.path.close();
    }

    public CanvasRounder(@NotNull CornersHolder cornersHolder) {
        this.cornersHolder = cornersHolder;
        this.path = new Path();
        this.rectF = new RectF(0.0F, 0.0F, 0.0F, 0.0F);
    }
}

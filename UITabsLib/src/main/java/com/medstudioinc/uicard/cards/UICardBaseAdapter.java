package com.medstudioinc.uicard.cards;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.medstudioinc.uicard.listener.OnCardViewClickListener;
import com.medstudioinc.uicard.listener.OnCardViewTouchListener;
import com.medstudioinc.uicard.listener.OnLongCardViewClickListener;

import java.util.ArrayList;
import java.util.List;

public abstract class UICardBaseAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public List<T> items;

    public OnLongCardViewClickListener longCardViewClickListener;
    public OnCardViewClickListener cardViewClickListener;
    public OnCardViewTouchListener cardViewTouchListener;

    public UICardBaseAdapter() {
    }

    public void addItems(List<T> items){
        this.items = items;
    }

    public void addItems(ArrayList<T> items){
        this.items = items;
    }

    private void add(List<T> items){
        for (T item : items){
            items.add(item);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = new UICardAllViews(viewGroup.getContext(), getUICardBottomBarColor());
        ConstraintLayout.LayoutParams viewParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        viewParams.topMargin = getUICardMargins();
        viewParams.leftMargin = getUICardMargins();
        viewParams.bottomMargin = getUICardMargins();
        viewParams.rightMargin = getUICardMargins();
        view.setLayoutParams(viewParams);
        return onCreateUICardHolder((ViewGroup) view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        onBindUICardHolder(viewHolder, i);
    }

    @Override
    public int getItemCount() {
        return items != null  ? items.size() : 0;
    }

    public abstract RecyclerView.ViewHolder onCreateUICardHolder(ViewGroup parent);
    public abstract void onBindUICardHolder(RecyclerView.ViewHolder viewHolder, int position);
    public abstract int getUICardMargins();
    public abstract boolean getUICardBottomBarColor();


    /**
     * Register a callback to be invoked when this view is clicked. If this view is not
     * clickable, it becomes clickable.
     *
     * @param listener The callback that will run
     *
     * @see #setOnCardViewClickListener(OnCardViewClickListener)
     */
    public void setOnCardViewClickListener(OnCardViewClickListener listener){
        this.cardViewClickListener = listener;
    }

    /**
     * Register a callback to be invoked when this view is clicked and held. If this view is not
     * long clickable, it becomes long clickable.
     *
     * @param listener The callback that will run
     *
     * @see #setOnCardViewLongClickListener(OnLongCardViewClickListener)
     */
    public void setOnCardViewLongClickListener(OnLongCardViewClickListener listener){
        this.longCardViewClickListener = listener;
    }

    /**
     * Register a callback to be invoked when this view is clicked and held. If this view is not
     * long touchable, it becomes long touchable.
     *
     * @param listener The callback that will run
     *
     * @see #setOnCardViewTouchListener(OnCardViewTouchListener)
     */

    public void setOnCardViewTouchListener(OnCardViewTouchListener listener){
        this.cardViewTouchListener = listener;
    }
}

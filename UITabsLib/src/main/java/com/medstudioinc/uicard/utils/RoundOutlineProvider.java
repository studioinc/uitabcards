package com.medstudioinc.uicard.utils;

import android.graphics.Outline;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.view.ViewOutlineProvider;

import org.jetbrains.annotations.NotNull;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class RoundOutlineProvider extends ViewOutlineProvider {
    private final CornersHolder cornersHolder;


    public RoundOutlineProvider(@NotNull CornersHolder cornersHolder) {
        this.cornersHolder = cornersHolder;
    }

    public RoundOutlineProvider(float cornerRadius) {
        this(new CornersHolder(cornerRadius, cornerRadius, cornerRadius, cornerRadius));
    }

    @Override
    public void getOutline(View view, Outline outline) {
        RectF rectF = new RectF(0f, 0f, (float)view.getMeasuredWidth(), (float)view.getMeasuredHeight());
        Path path = new Path();
        Extensions.addRoundRectWithRoundCorners(path, rectF, this.cornersHolder);
        path.close();
        outline.setConvexPath(path);
    }


}

package com.medstudioinc.uitabs;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.medstudioinc.uicard.cards.UICardAllViews;
import com.medstudioinc.uicard.cards.UICardBaseAdapter;
import com.medstudioinc.uicard.utils.CornerType;


public class UICardAdapter extends UICardBaseAdapter<UICardModel> {
    boolean isBottomSection;

    @Override
    public RecyclerView.ViewHolder onCreateUICardHolder(ViewGroup parent) {
        return new ItemUICardHolder(parent);
    }

    @Override
    public void onBindUICardHolder(RecyclerView.ViewHolder viewHolder, int position) {
        UICardModel uiCardModel = items.get(position);
        ItemUICardHolder holder = (ItemUICardHolder) viewHolder;
        holder.uiCardAllViews.setUICardText(uiCardModel.getTitle());
        holder.uiCardAllViews.setUICardThumbnail(uiCardModel.getIcon());
        holder.uiCardAllViews.setUICardGradientColors(0.55f, uiCardModel.getColors()[0], uiCardModel.getColors()[1]);
        holder.uiCardAllViews.setUICardBottomSectionGradientColors(0.98f, uiCardModel.getColors()[1],  uiCardModel.getColors()[0]);
        holder.uiCardAllViews.setUICardRoundedCornerRadius(20, CornerType.ALL);
    }

    @Override
    public int getUICardMargins() {
        return 3;
    }

    @Override
    public boolean getUICardBottomBarColor() {
        return this.isBottomSection;
    }

    public class ItemUICardHolder extends RecyclerView.ViewHolder {

        UICardAllViews uiCardAllViews;
        public ItemUICardHolder(@NonNull final View itemView) {
            super(itemView);
            uiCardAllViews = (UICardAllViews) itemView;
            uiCardAllViews.setBottomSectionBar(isBottomSection);
            uiCardAllViews.setOnCardViewClickListener(v->{
                if (cardViewClickListener != null){
                    cardViewClickListener.onClick(v);
                }
            });
            uiCardAllViews.setOnCardViewLongClickListener(v->  {
                if (longCardViewClickListener != null){
                    longCardViewClickListener.onLongClick(v);
                }
                return false;
            });
            uiCardAllViews.setOnCardViewTouchListener((v, e)->{
                if (cardViewTouchListener != null){
                    cardViewTouchListener.onTouch(v, e);
                }
                return false;
            });
        }

    }


    public void setBottomBarColor(boolean section){
        this.isBottomSection = section;
    }

}

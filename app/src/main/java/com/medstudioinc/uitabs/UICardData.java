package com.medstudioinc.uitabs;

import java.util.ArrayList;

public class UICardData {
    public static ArrayList<UICardModel> cards(){
        ArrayList<UICardModel> UICardModels = new ArrayList<>();

        UICardModels.add(new UICardModel(0, R.drawable.conding, "Coding", new String[]{"#EB5757", "#000000"}));
        UICardModels.add(new UICardModel(1, R.drawable.ideas, "Ideas", new String[]{"#6441A5", "#2a0845"}));
        UICardModels.add(new UICardModel(2, R.drawable.drinks, "Drink", new String[]{"#34e89e", "#0f3443"}));
        UICardModels.add(new UICardModel(3, R.drawable.food, "Food", new String[]{"#ED213A", "#93291E"}));
        UICardModels.add(new UICardModel(4, R.drawable.health, "Health", new String[]{"#4e54c8", "#8f94fb"}));
        UICardModels.add(new UICardModel(5, R.drawable.lifestyle, "Lifestyle", new String[]{"#37ecba", "#72afd3"}));
        UICardModels.add(new UICardModel(6, R.drawable.sport, "Sport", new String[]{"#6a11cb", "#2575fc"}));
        UICardModels.add(new UICardModel(7, R.drawable.yoga, "Yoga", new String[]{"#8E2DE2", "#4A00E0"}));
        UICardModels.add(new UICardModel(8, R.drawable.surgery, "Hopital", new String[]{"#434343", "#000000"}));
        UICardModels.add(new UICardModel(9, R.drawable.addiction, "Addiction", new String[]{"#4286f4", "#373B44"}));
        UICardModels.add(new UICardModel(10, R.drawable.nature, "Beauty", new String[]{"#1488CC", "#2B32B2"}));
        UICardModels.add(new UICardModel(11, R.drawable.office, "Office", new String[]{"#FFE000", "#799F0C"}));

        return UICardModels;
    }
}

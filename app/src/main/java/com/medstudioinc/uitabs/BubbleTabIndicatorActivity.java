package com.medstudioinc.uitabs;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.medstudioinc.uicard.indicator.UIBubbleTabIndicator;
import com.medstudioinc.uicard.indicator.UIBubbleTabIndicatorHost;
import com.medstudioinc.uicard.listener.UIBubbleTabIndicatorListener;

public class BubbleTabIndicatorActivity extends AppCompatActivity implements UIBubbleTabIndicatorListener {
    private UIBubbleTabIndicatorHost tabHost;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        RelativeLayout relativeLayout = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams
                = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT);
        relativeLayout.setLayoutParams(layoutParams);

        relativeLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));

        tabHost = new UIBubbleTabIndicatorHost(this);
        tabHost.setId(R.id.tabs);
        tabHost.setUITabCornerRadius(10);
        tabHost.setUITabCardWidthAndHeight(60, 40);
        tabHost.setUITabCardMargins(4,10,4,10);
        tabHost.setUITabBackgroundColor(Color.TRANSPARENT);
        tabHost.setUITabSelectorColor(Color.WHITE);
        tabHost.setUITabTextColor(Color.WHITE);
        tabHost.setUITabCardTextSize(12);
        tabHost.setUITabCardTextFont("fonts/future.ttf");
        tabHost.setUITabListener(this);

        relativeLayout.addView(tabHost);


        setContentView(relativeLayout, layoutParams);

        addTabs();
    }

    private void addTabs(){
        for (int i = 0; i < getTabTitles().length; i++){
            tabHost.addUITabCard(tabHost.newUITabCard()
                    .setUITabCardText(getTabTitles()[i])
                     .setUITabCardBackgroundColor(1, Color.parseColor("#"+getTabColors()[i][0]) ));
                     //.setUITabCardBackgroundGradientColors(new GradientColors(0.99f, getTabColors()[i][0], getTabColors()[i][1])));
        }
    }

    private String[] getTabTitles(){
        return new String[] {
                "Coding",
                "Ideas",
                "Drink",
                "Food",
                "Health",
                "Lifestyle",
                "Sport",
                "Yoga",
                "Hopital",
                "Addiction",
                "Beauty",
                "Office"
        };
    }

    private String[][] getTabColors(){
        return new String[][] {
                {"EB5757", "000000"},
                {"6441A5", "2a0845"},
                {"43cea2", "185a9d"},
                {"fd1d1d", "fcb045"},
                {"c43a30", "874da2"},
                {"FD8BD9", "7742B2"},
                {"243949", "517fa4"},
                {"243949", "517fa4"},
                {"434343", "000000"},
                {"e52d27", "b31217"},
                {"f83600", "f9d423"},
                {"4481eb", "04befe"}
        };
    }

    @Override
    public void onTabSelected(UIBubbleTabIndicator tab) {
        tabHost.setUITabSelectedNavigationItem(tab.getUITabCardPosition());
    }

    @Override
    public void onTabReselected(UIBubbleTabIndicator tab) {

    }

    @Override
    public void onTabUnselected(UIBubbleTabIndicator tab) {

    }
}

package com.medstudioinc.uicard.utils;


import android.support.annotation.FloatRange;


public class GradientColors {
    private String[] originalColor= new String[2];
    private float alpha;

    public GradientColors() {
    }

    public GradientColors(@FloatRange(from = 0, to = 1.0f) float alpha, String firstColor, String secondColor) {
        setAlpha(alpha);
        setFirstColor(firstColor);
        setSecondColor(secondColor);
    }

    public GradientColors setFirstColor(String firstColor){
        if (firstColor != null){
            if (firstColor.startsWith("#")){
                this.originalColor[0] = firstColor;
            } else {
                this.originalColor[0] = "#" + firstColor;
            }
        }
        return this;
    }

    public GradientColors setSecondColor(String secondColor){
        if (secondColor != null){
            if (secondColor.startsWith("#")){
                this.originalColor[1] = secondColor;
            } else {
                this.originalColor[1] = "#" + secondColor;
            }
        }
        return this;
    }

    public String startColor(){
        return this.originalColor[0];
    }

    public String centerColor(){
        return this.originalColor[2];
    }

    public String endColor(){
        return this.originalColor[1];
    }

    public float alpha(){
        return alpha;
    }

    public void setAlpha(float alpha){
        this.alpha = alpha;
    }

    public int length(){
        return this.originalColor.length;
    }

}

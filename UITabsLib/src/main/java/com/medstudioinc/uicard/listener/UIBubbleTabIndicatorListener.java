package com.medstudioinc.uicard.listener;


import com.medstudioinc.uicard.indicator.UIBubbleTabIndicator;

public interface UIBubbleTabIndicatorListener {
	 void onTabSelected(UIBubbleTabIndicator tab);

	 void onTabReselected(UIBubbleTabIndicator tab);
	
	 void onTabUnselected(UIBubbleTabIndicator tab);
}

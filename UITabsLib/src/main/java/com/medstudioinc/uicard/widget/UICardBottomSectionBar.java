package com.medstudioinc.uicard.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.ColorRes;
import android.support.annotation.FloatRange;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.medstudioinc.uicard.utils.Utils;

public class UICardBottomSectionBar extends View {
    public UICardBottomSectionBar(Context context) {
        super(context);
    }

    public UICardBottomSectionBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public UICardBottomSectionBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setGrayedOut(boolean grayedOut, Paint grayscalePaint) {
        if (grayedOut) {
            setLayerType(View.LAYER_TYPE_HARDWARE, grayscalePaint);
        } else {
            setLayerType(View.LAYER_TYPE_NONE, null);
        }
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionBackgroundColor(String color){
        setBackgroundColor(Color.parseColor(color));
    }


    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionBackgroundColor(@FloatRange(from = 0, to = 1.0) float alpha, String color){
        setBackgroundColor(Color.parseColor(color));
        setAlpha(alpha);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionBackgroundColor(@ColorRes int resid){
        setBackgroundColor(getContext().getResources().getColor(resid));
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionBackgroundColor(@FloatRange(from = 0, to = 1.0) float alpha, @ColorRes int resid){
        setBackgroundColor(getContext().getResources().getColor(resid));
        setAlpha(alpha);
    }


    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionGradientColors(String startColor, String endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] {Color.parseColor(startColor), Color.parseColor(endColor)});
        setBackgroundDrawable(gd);
    }


    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, String startColor, String endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[] {
                Color.parseColor(Utils.setColorAlpha(100, startColor)),
                Color.parseColor(Utils.setColorAlpha(100, endColor))
        });
        gd.setCornerRadius(2);
        setBackgroundDrawable(gd);
        setAlpha(alpha);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, @ColorRes int startColor, @ColorRes int endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[] {
                startColor,
                endColor
        });
        setBackgroundDrawable(gd);
        setAlpha(alpha);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionGradientColors(String startColor, String centerColor, String endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] {
                Color.parseColor(startColor),
                Color.parseColor(centerColor),
                Color.parseColor(endColor)
        });
        setBackgroundDrawable(gd);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, String startColor, String centerColor, String endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] {
                Color.parseColor(Utils.setColorAlpha(100, startColor)),
                Color.parseColor(Utils.setColorAlpha(100, centerColor)),
                Color.parseColor(Utils.setColorAlpha(100, endColor))
        });
        setBackgroundDrawable(gd);
        setAlpha(alpha);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, @ColorRes int startColor, @ColorRes int centerColor, @ColorRes int endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] {
                startColor,
                centerColor,
                endColor
        });
        setBackgroundDrawable(gd);
        setAlpha(alpha);
    }

    public void setUICardBottomSectionViewFilter(boolean filter, String color){
        final int semiTransparentGrey = Color.argb(200, 185, 185, 255);
        PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(semiTransparentGrey, PorterDuff.Mode.SRC_ATOP);
        Paint paint = new Paint();
        paint.setColorFilter(colorFilter);
        setGrayedOut(filter, paint);
    }

    /**
     * Set the layout parameters associated with this view. These supply
     * parameters to the <i>parent</i> of this view specifying how it should be
     * arranged. There are many subclasses of ViewGroup.LayoutParams, and these
     * correspond to the different subclasses of ViewGroup that are responsible
     * for arranging their children.
     *
     * @param params The layout parameters for this view, cannot be null
     */
    public void setUICardSectionViewLayoutParams(ViewGroup.LayoutParams params){
        setLayoutParams(params);
    }
}

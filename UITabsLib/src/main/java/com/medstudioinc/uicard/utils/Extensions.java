package com.medstudioinc.uicard.utils;

import android.content.res.TypedArray;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Build;
import android.view.View;

import org.jetbrains.annotations.NotNull;


public class Extensions {

    public static final void updateOutlineProvider(@NotNull View view, @NotNull CornersHolder cornersHolder) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setOutlineProvider(new RoundOutlineProvider(cornersHolder));
        }

    }

    public static final void updateOutlineProvider(@NotNull View view, float cornerRadius) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setOutlineProvider(new RoundOutlineProvider(cornerRadius));
        }

    }

    public static final void addRoundRectWithRoundCorners(
            @NotNull Path path,
            @NotNull RectF rectF,
            @NotNull CornersHolder cornersHolder) {

        addRoundRectWithRoundCorners(path, rectF,
                cornersHolder.getTopLeftCornerRadius(),
                cornersHolder.getTopRightCornerRadius(),
                cornersHolder.getBottomRightCornerRadius(),
                cornersHolder.getBottomLeftCornerRadius());
    }

    public static final void addRoundRectWithRoundCorners(
            @NotNull Path path,
            @NotNull RectF rectF,
            float topLeftCornerRadius,
            float topRightCornerRadius,
            float bottomRightCornerRadius,
            float bottomLeftCornerRadius) {

        path.addRoundRect(rectF, new float[]{
                topLeftCornerRadius,
                topLeftCornerRadius,
                topRightCornerRadius,
                topRightCornerRadius,
                bottomRightCornerRadius,
                bottomRightCornerRadius,
                bottomLeftCornerRadius,
                bottomLeftCornerRadius},
                Path.Direction.CW);
    }

    @NotNull
    public static final CornersHolder getCornerRadius(
            @NotNull TypedArray typedArray,
            int attrCornerRadius,
            int attrTopLeftCornerRadius,
            int attrTopRightCornerRadius,
            int attrBottomRightCornerRadius,
            int attrBottomLeftCornerRadius) {

        float cornerRadius = typedArray.getDimension(attrCornerRadius, 0.0F);
        float topLeftCornerRadius = typedArray.getDimension(attrTopLeftCornerRadius, cornerRadius);
        float topRightCornerRadius = typedArray.getDimension(attrTopRightCornerRadius, cornerRadius);
        float bottomRightCornerRadius = typedArray.getDimension(attrBottomRightCornerRadius, cornerRadius);
        float bottomLeftCornerRadius = typedArray.getDimension(attrBottomLeftCornerRadius, cornerRadius);
        return new CornersHolder(topLeftCornerRadius, topRightCornerRadius, bottomRightCornerRadius, bottomLeftCornerRadius);
    }
}

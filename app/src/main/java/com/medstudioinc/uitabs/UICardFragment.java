package com.medstudioinc.uitabs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class UICardFragment extends Fragment {
    private RecyclerView rv;
    private UICardAdapter adapter;

    public static UICardFragment newInstance(){
        return new UICardFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return new RecyclerView(getContext());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setPadding(4, 4, 4, 4);
        rv = (RecyclerView) view;
        ViewPager.LayoutParams params = new ViewPager.LayoutParams();
        params.width = ViewPager.LayoutParams.MATCH_PARENT;
        params.height = ViewPager.LayoutParams.MATCH_PARENT;
        rv.setLayoutParams(params);

        adapter = new UICardAdapter();
        rv.setLayoutManager(new GridLayoutManager(getContext(), 2));
        rv.setHasFixedSize(true);
        rv.setItemViewCacheSize(20);
        rv.setDrawingCacheEnabled(true);
        rv.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rv.setNestedScrollingEnabled(false);
        adapter.setHasStableIds(false);
        adapter.setBottomBarColor(false);
        rv.setAdapter(adapter);
        adapter.addItems(UICardData.cards());

        adapter.setOnCardViewClickListener((v)-> {
            Toast.makeText(getContext(), "Short click", Toast.LENGTH_SHORT).show();
        });
        adapter.setOnCardViewLongClickListener((v)-> {
            Toast.makeText(getContext(), "Long click and consumed", Toast.LENGTH_SHORT).show();
            return false;
        });
        adapter.setOnCardViewTouchListener((v, event)->{
            Toast.makeText(getContext(), "Touch event", Toast.LENGTH_SHORT);
            return false;
        });

    }
}

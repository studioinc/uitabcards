package com.medstudioinc.uicard.listener;

import android.view.View;

public interface OnLongCardViewClickListener {
    boolean onLongClick(View v);
}

package com.medstudioinc.uitabs;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class UICardPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<String> mTitiles = new ArrayList<String>();
    private ArrayList<Fragment> mFragments = new ArrayList<Fragment>();

    public UICardPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public UICardPagerAdapter(FragmentManager fm, Fragment fragments, String titles) {
        super(fm);
        this.mFragments.add(fragments);
        this.mTitiles.add(titles);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    public void add(Fragment fragment){
        mFragments.add(fragment);
    }

    public void add(Fragment fragment, String title){
        mFragments.add(fragment);
        mTitiles.add(title);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitiles.get(position);
    }

    public ArrayList<Fragment> getFragments() {
        return mFragments;
    }

    public ArrayList<String> getTitiles() {
        return mTitiles;
    }

    @Override
    public float getPageWidth(int position) {
        return super.getPageWidth(position);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
}

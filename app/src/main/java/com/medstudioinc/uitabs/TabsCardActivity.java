package com.medstudioinc.uitabs;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.medstudioinc.uicard.tabs.UITabsCard;
import com.medstudioinc.uicard.tabs.UITabsCardHost;
import com.medstudioinc.uicard.listener.UITabsCardListener;
import com.medstudioinc.uicard.utils.CornerType;
import com.medstudioinc.uicard.utils.GradientColors;

public class TabsCardActivity extends AppCompatActivity implements UITabsCardListener {
    private UITabsCardHost tabHost;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        RelativeLayout relativeLayout = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams
                = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT);
        relativeLayout.setLayoutParams(layoutParams);

        relativeLayout.setBackgroundColor(Color.parseColor("#EFF0F1"));

        tabHost = new UITabsCardHost(this);
        tabHost.setId(R.id.tabs);
        tabHost.setUITabCornerRadius(20, CornerType.ALL);
        tabHost.setUITabCardWidthAndHeight(90, 70);
        tabHost.setUITabCardMargins(6,12,6,12);
        tabHost.setUITabBackgroundColor(Color.TRANSPARENT);
        tabHost.setUITabSelectorColor(Color.WHITE);
        tabHost.setUITabTextColor(Color.WHITE);
        tabHost.setUITabCardTextSize(12);
        tabHost.setUITabCardTextFont("fonts/future.ttf");
        tabHost.setUITabListener(this);

        relativeLayout.addView(tabHost);


        setContentView(relativeLayout, layoutParams);
        addTabs();
    }

    private void addTabs(){
        for (int i = 0; i < getTabTitles().length; i++){
            tabHost.addUITabCard(tabHost.newUITabCard()
                    .setUITabCardText(getTabTitles()[i])
                    .setTabCardThumbnail(getTabThumbnails()[i])
                    .setUITabCardBackgroundGradientColors(new GradientColors(0.55f, getTabColors()[i][0], getTabColors()[i][1])));
            //.setUITabCardBackgroundGradientColors(0.25f, new int[]{R.color.colorAccent, R.color.colorPrimaryDark}));
        }
    }


    private String[] getTabTitles(){
        return new String[] {
                "Coding",
                "Ideas",
                "Drink",
                "Food",
                "Health",
                "Lifestyle",
                "Sport",
                "Yoga",
                "Hopital",
                "Addiction",
                "Beauty",
                "Office"
        };
    }

    private String[][] getTabColors(){
        return new String[][] {
                {"EB5757", "000000"},
                {"6441A5", "2a0845"},
                {"43cea2", "185a9d"},
                {"fd1d1d", "fcb045"},
                {"c43a30", "874da2"},
                {"FD8BD9", "7742B2"},
                {"243949", "517fa4"},
                {"243949", "517fa4"},
                {"434343", "000000"},
                {"e52d27", "b31217"},
                {"f83600", "f9d423"},
                {"4481eb", "04befe"}
        };
    }

    private int[] getTabThumbnails(){
        return new int[] {
                R.drawable.conding,
                R.drawable.ideas,
                R.drawable.drinks,
                R.drawable.food,
                R.drawable.health,
                R.drawable.lifestyle,
                R.drawable.sport,
                R.drawable.yoga,
                R.drawable.surgery,
                R.drawable.addiction,
                R.drawable.nature,
                R.drawable.office
        };
    }

    @Override
    public void onTabSelected(UITabsCard tab) {
        tabHost.setUITabSelectedNavigationItem(tab.getUITabCardPosition());
    }

    @Override
    public void onTabReselected(UITabsCard tab) {

    }

    @Override
    public void onTabUnselected(UITabsCard tab) {

    }
}

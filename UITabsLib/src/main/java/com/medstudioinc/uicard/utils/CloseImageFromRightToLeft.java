package com.medstudioinc.uicard.utils;

import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class CloseImageFromRightToLeft extends Animation {
    private final int newWidth;
    private final int newHeight;
    private final int originalWidth;
    private ImageView mView;

    public CloseImageFromRightToLeft(ImageView v, int[] widthAndHeight) {
        mView = v;
        this.newWidth = widthAndHeight[0] / 3;
        this.originalWidth = widthAndHeight[0];
        this.newHeight = widthAndHeight[1];
    }

    @Override
    public void initialize(int width, int height, int parentWidth, int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }

    @Override
    public void applyTransformation(float interpolatedTime, Transformation t) {
        int newWidth = originalWidth + (int) ((this.newWidth - originalWidth) * interpolatedTime);


        LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(newWidth, this.newHeight);
        mView.setLayoutParams(parms);
        mView.requestLayout();
    }
}
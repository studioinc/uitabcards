package com.medstudioinc.uicard;

public interface UITabListener {
	public void onTabSelected(UITab tab);
	
	public void onTabReselected(UITab tab);
	
	public void onTabUnselected(UITab tab);
}

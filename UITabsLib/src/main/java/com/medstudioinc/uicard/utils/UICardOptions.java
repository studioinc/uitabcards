package com.medstudioinc.uicard.utils;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntRange;
import android.support.annotation.StringRes;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import com.medstudioinc.uitabslib.R;
import java.util.Random;


public class UICardOptions {
    private Context context;
    private Resources resources;

    private GradientColors gradientColors;
    private TextShadowLayer textShadowLayer;
    private UICardLayoutParams uiCardLayoutParams;

    private Drawable cardThumbnailRes;
    public String cardTitleText;
    public int cardTitleColor;
    public float cardTitleSize;
    private Typeface cardTitleTypeface;
    private int cardBackgroundSolidColor;

    private float cardRoundedCornersRadius;

    private boolean cardHasBluring;

    private boolean rippleDelayClick;
    private boolean rippleOverlay;
    private boolean rippleHover;
    private boolean rippleInAdapter;
    private boolean ripplePersistent;

    private Integer rippleAlpha;
    private int defaultRippleAlpha;
    private int rippleBackground;
    private int rippleColor;
    private int rippleDuration = 300;
    private int rippleRadius;
    private int rippleDiameter;
    private int rippleFadeDuration;

    private boolean isThumbnailColorFilter;
    private PorterDuffColorFilter thumbnailColorFilter;

    private int cardWidth;
    private int cardHeight;


    public UICardOptions(Context context) {
        this.context = context;
        this.resources = context.getResources();
        defaultValues();
    }

    private void defaultValues(){
        setCardMargins(0);
        setCardWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setCardHeight(DisplayMetrics.DENSITY_360); // DisplayMetrics.DENSITY_HIGH / DENSITY_360 / DENSITY_XXXHIGH
        setCardHasBluring(false);
        setTitle("Item");
        setTitleShadow(0, 0, 0, Color.TRANSPARENT);
        setTitleSize(12);
        setThumbnail(R.drawable.addiction);
        //setThumbnail(generatorThumbnail());
        setTitleColor(Color.WHITE);
        setTitleTypeface("fonts/future.ttf");
        setThumbnailColorFilter(false, 50, 185, 185, 255);
        setRoundedCorners(1);
        GradientColors colors = new GradientColors();
        colors.setFirstColor("#009688");
        colors.setSecondColor("#3C3F41");
        colors.setAlpha(30);
        setBackgroundGradientColors(colors);
    }

    private int generatorThumbnail(){
        int[] thumbnails = new int[] {
                R.drawable.conding,
                R.drawable.office,
                R.drawable.drinks,
                R.drawable.food,
                R.drawable.lifestyle,
                R.drawable.health,
                R.drawable.office,
                R.drawable.nature,
                R.drawable.sport,
                R.drawable.surgery,
                R.drawable.ideas,
                R.drawable.yoga
        };

        int random = new Random().nextInt(thumbnails.length);

        return thumbnails[random];
    }

    public Resources getResources(){
        return resources;
    }

    public Context getContext() {
        return context;
    }

    public void setCardWidth(int width){
        this.cardWidth = width;
    }

    public void setCardHeight(int height){
        this.cardHeight = height;
    }

    public void setTitle(@StringRes int strRes){
        this.cardTitleText = getResources().getString(strRes);
    }

    public void setTitle(String strRes){
        this.cardTitleText = strRes;
    }

    public void setTitleColor(int colorRes){
        this.cardTitleColor = colorRes;
    }

    public void setTitleColor(ColorStateList colorRes){
        this.cardTitleColor = colorRes.getDefaultColor();
    }

    public void setTitleColor(String colorRes){
        this.cardTitleColor = Color.parseColor(colorRes);
    }

    public void setTitleSize(float textSize){
        this.cardTitleSize = textSize;

    }

    public void setTitleTypeface(Typeface typeface){
        this.cardTitleTypeface = typeface;
    }

    public void setTitleTypeface(String typefacePath){
        this.cardTitleTypeface = Typeface.createFromAsset(getContext().getAssets(), typefacePath);
    }

    public void setTitleShadow(float radius, float dx, float dy, int color){
        this.textShadowLayer = new TextShadowLayer(radius, dx, dy, color);
    }

    public void setBackgroundSolidColor(@ColorInt int solidColorRes){
        this.cardBackgroundSolidColor = solidColorRes;

    }

    public void setBackgroundSolidColor(String solidColorRes){
        this.cardBackgroundSolidColor = Color.parseColor(solidColorRes);
    }


    public void setBackgroundGradientColors(GradientColors gradientColorRes){
        this.gradientColors = gradientColorRes;
    }


    public void setThumbnail(@DrawableRes int drawable){
        this.cardThumbnailRes = getResources().getDrawable(drawable);
    }

    public void setThumbnail(Drawable drawable){
        this.cardThumbnailRes = drawable;
    }

    public void setThumbnailColorFilter(boolean filter){
        this.isThumbnailColorFilter = filter;
    }

    public void setThumbnailColorFilter(boolean filter,
                                        @IntRange(from = 0, to = 255) int alpha,
                                        @IntRange(from = 0, to = 255) int red,
                                        @IntRange(from = 0, to = 255) int green,
                                        @IntRange(from = 0, to = 255) int blue){
        this.isThumbnailColorFilter = filter;

        if (this.isThumbnailColorFilter()) {
            final int semiTransparentGrey = Color.argb(alpha, red, green, blue);
            int highlightColor = getContext().getResources().getColor(android.R.color.background_dark);
            PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(semiTransparentGrey, PorterDuff.Mode.SRC_ATOP);

            Paint redHighLight = new Paint();
            redHighLight.setColorFilter(colorFilter);
            redHighLight.setAlpha(190);

            thumbnailColorFilter = colorFilter;
        }
    }

    public void setThumbnailColorFilter(boolean filter, int color){
        this.isThumbnailColorFilter = filter;
        //int hexColor = getContext().getResources().getColor(color);
        int hexColor = color;

        if (this.isThumbnailColorFilter()) {
            PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(hexColor, PorterDuff.Mode.DARKEN);

            Paint redHighLight = new Paint();
            redHighLight.setColorFilter(colorFilter);
            redHighLight.setAlpha(190);
            thumbnailColorFilter = colorFilter;
        }
    }

    public void setCardMargins(int margins){
        this.uiCardLayoutParams = new UICardLayoutParams(margins);
    }

    public void setCardMargins(int top, int left, int right, int bottom){
        this.uiCardLayoutParams = new UICardLayoutParams(top, left, left, bottom);
    }

    public UICardLayoutParams getUiCardLayoutParams() {
        return uiCardLayoutParams;
    }

    public TextShadowLayer getTextShadowLayer() {
        return textShadowLayer;
    }

    public boolean isThumbnailColorFilter(){
        return isThumbnailColorFilter;
    }

    public void setRoundedCorners(float roundedCorners){
        this.cardRoundedCornersRadius = roundedCorners;
    }

    public void setCardHasBluring(boolean hasBluring) {
        this.cardHasBluring = hasBluring;
    }

    public boolean isCardHasBluring(){
        return this.cardHasBluring;
    }

    public void setRippleDelayClick(boolean delayClick){
        this.rippleDelayClick = delayClick;
    }

    public void setRippleAlpha(Integer rippleAlpha){
        this.rippleAlpha = rippleAlpha;
    }

    public void setRippleBackground(int rippleBackground){
        this.rippleBackground = rippleBackground;
    }

    public void setRippleOverlay(boolean rippleOverlay){
        this.rippleOverlay = rippleOverlay;
    }

    public void setRippleHover(boolean rippleHover){
        this.rippleHover = rippleHover;
    }

    public void setRippleColor(int rippleColor){
        this.rippleColor = rippleColor;
    }

    public void setDefaultRippleAlpha(int defaultRippleAlpha){
        this.defaultRippleAlpha = defaultRippleAlpha;
    }

    public void setRippleDuration(int rippleDuration){
        this.rippleDuration = rippleDuration;
    }

    public void setRippleRadius(int rippleRadius){
        this.rippleRadius = rippleRadius;
    }

    public void setRippleDiameter(int rippleDiameter){
        this.rippleDiameter = rippleDiameter;
    }

    public void setRippleFadeDuration(int rippleFadeDuration){
        this.rippleFadeDuration = rippleFadeDuration;
    }

    public void setRippleInAdapter(boolean rippleInAdapter){
        this.rippleInAdapter = rippleInAdapter;
    }

    public void setRipplePersistent(boolean ripplePersistent){
        this.ripplePersistent = ripplePersistent;
    }


    public int getCardMinWidth(){
        return cardWidth;
    }

    public int getCardMinHeight(){
        return cardHeight;
    }

    public int getTitleColor() {
        return this.cardTitleColor;
    }

    public float getTitleSize() {
        return this.cardTitleSize;
    }

    public Typeface getCardTitleTypeface() {
        return cardTitleTypeface;
    }

    public int getBackgroundSolidColor(){
        return this.cardBackgroundSolidColor;
    }

    public GradientColors getGradientColors() {
        return gradientColors;
    }

    public float getRoundedCornersRadius() {
        return this.cardRoundedCornersRadius;
    }

    public Drawable getThumbnailRes() {
        return this.cardThumbnailRes;
    }

    public int getShadowColor() {
        return this.cardBackgroundSolidColor;
    }

    public String getTitleText() {
        return cardTitleText;
    }

    public PorterDuffColorFilter getThumbnailColorFilter() {
        return thumbnailColorFilter;
    }

    public boolean isRippleDelayClick() {
        return rippleDelayClick;
    }

    public boolean isRippleOverlay() {
        return rippleOverlay;
    }

    public boolean isRippleHover() {
        return rippleHover;
    }

    public boolean isRippleInAdapter() {
        return rippleInAdapter;
    }

    public boolean isRipplePersistent() {
        return ripplePersistent;
    }

    public Integer getRippleAlpha() {
        return rippleAlpha;
    }

    public int getDefaultRippleAlpha() {
        return defaultRippleAlpha;
    }

    public int getRippleBackground() {
        return rippleBackground;
    }

    public int getRippleColor() {
        return rippleColor;
    }

    public int getRippleDuration() {
        return rippleDuration;
    }

    public int getRippleRadius() {
        return rippleRadius;
    }

    public int getRippleDiameter() {
        return rippleDiameter;
    }

    public int getRippleFadeDuration() {
        return rippleFadeDuration;
    }
}

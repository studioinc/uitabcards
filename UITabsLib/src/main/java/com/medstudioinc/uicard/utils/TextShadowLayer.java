package com.medstudioinc.uicard.utils;

public class TextShadowLayer {
    private float mShadowRadius, mShadowDx, mShadowDy;
    private int mShadowColor;

    public TextShadowLayer(float radius, float dx, float dy, int color) {
        mShadowRadius = radius;
        mShadowDx = dx;
        mShadowDy = dy;
        mShadowColor = color;
    }

    public float getTextShadowRadius() {
        return mShadowRadius;
    }

    public float getTextShadowDx() {
        return mShadowDx;
    }

    public float getTextShadowDy() {
        return mShadowDy;
    }

    public int getTextShadowColor() {
        return mShadowColor;
    }
}

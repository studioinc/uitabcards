package com.medstudioinc.uicard.listener;

import android.graphics.Canvas;

public interface DrawingListener {
    void onProcesses(Canvas canvas);
}

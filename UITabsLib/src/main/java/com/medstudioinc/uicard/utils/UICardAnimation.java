package com.medstudioinc.uicard.utils;

import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.view.View;
import android.view.animation.Interpolator;


public class UICardAnimation {
    private View viewGroup;
    private UICardAnimationListener animationListener;
    private Interpolator interpolator;
    private int duration;
    private float scale_x;
    private float scale_y;

    public UICardAnimation(View view) {
        this.viewGroup = view;
        defualtValues();
    }

    public static UICardAnimation setup(View view){
        return new UICardAnimation(view);
    }

    private void defualtValues(){
        this.interpolator = new CustomInterpolator();
        this.duration = 200;
        this.scale_x = 0.93F;
        this.scale_y = 0.93F;
    }

    public UICardAnimation setDuration(int val){
        this.duration = val;
        return this;
    }

    public UICardAnimation scaleX(float val){
        this.scale_x = val;
        return this;
    }

    public UICardAnimation scaleY(float val){
        this.scale_y = val;
        return this;
    }

    public UICardAnimation setInterpolator(Interpolator val){
        this.interpolator = val;
        return this;
    }

    public UICardAnimation commit(UICardAnimationListener listener){
        this.animationListener = listener;
        ViewCompat.animate(viewGroup)
                .setDuration(duration)
                .scaleX(scale_x)
                .scaleY(scale_y)
                .setInterpolator(interpolator)
                .setListener(new ViewPropertyAnimatorListener() {
                    @Override
                    public void onAnimationStart(View view) {
                        if (animationListener != null){
                            animationListener.onAnimationStart();
                        }
                    }

                    @Override
                    public void onAnimationEnd(View view) {
                        if (animationListener != null){
                            animationListener.onAnimationEnd();
                        }
                    }

                    @Override
                    public void onAnimationCancel(View view) {
                        if (animationListener != null){
                            animationListener.onAnimationCancel();
                        }
                    }
                });

        return this;
    }


    public class CustomInterpolator implements android.view.animation.Interpolator {

        private final float mCycles = 0.5f;

        @Override
        public float getInterpolation(final float input) {
            return (float) Math.sin(2.0f * mCycles * Math.PI * input);
        }
    }

    public static abstract class UICardAnimationListener {
        public void onAnimationStart(){}
        public void onAnimationEnd(){}
        public void onAnimationCancel(){}
    }


}

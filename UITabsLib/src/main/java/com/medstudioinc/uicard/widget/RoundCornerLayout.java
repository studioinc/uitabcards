package com.medstudioinc.uicard.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import com.medstudioinc.uicard.utils.CanvasRounder;
import com.medstudioinc.uicard.utils.CornerType;
import com.medstudioinc.uicard.utils.CornersHolder;
import com.medstudioinc.uicard.utils.Extensions;
import com.medstudioinc.uitabslib.R;

import org.jetbrains.annotations.NotNull;

public class RoundCornerLayout extends ConstraintLayout{
    CanvasRounder canvasRounder;
    CornersHolder cornersHolder;

    public RoundCornerLayout(Context context) {
        super(context);
        init();
    }

    public RoundCornerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RoundCornerLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.RoundCornerLayout, 0, 0);
        try {
            cornersHolder = Extensions.getCornerRadius(typedArray,
                    R.styleable.RoundCornerLayout_rounded_korner_radius,
                    R.styleable.RoundCornerLayout_top_left_corner_radius,
                    R.styleable.RoundCornerLayout_top_right_corner_radius,
                    R.styleable.RoundCornerLayout_bottom_right_corner_radius,
                    R.styleable.RoundCornerLayout_bottom_left_corner_radius);
            Extensions.updateOutlineProvider(this, cornersHolder);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
                setLayerType(LAYER_TYPE_SOFTWARE, null);
            }
        } finally {
            typedArray.recycle();
        }

        init();

    }

    private void init(){
        cornersHolder = new CornersHolder(0, 0, 0, 0);
        canvasRounder = new CanvasRounder(cornersHolder);
    }

    @Override
    public boolean isInEditMode() {
        return true;
    }

    @Override
    protected void onSizeChanged(int currentWidth, int currentHeight, int oldw, int oldh) {
        super.onSizeChanged(currentWidth, currentHeight, oldw, oldh);
        canvasRounder.updateSize(currentWidth, currentHeight);
    }

    @Override
    public void draw(Canvas canvas) {
        this.canvasRounder.round(canvas, (draw)->{
            super.draw(draw);
        });
    }

    @Override
    public void dispatchDraw(Canvas canvas) {
        this.canvasRounder.round(canvas, (draw)->{
            super.dispatchDraw(draw);
        });
    }

    public void setCornerRadius(float cornerRadius, @NotNull CornerType cornerType) {
        if (cornerRadius > 0){
            switch(cornerType) {
                case ALL:
                    this.canvasRounder.setCornerRadius(cornerRadius);
                    break;
                case TOP_LEFT:
                    this.canvasRounder.setTopLeftCornerRadius(cornerRadius);
                    break;
                case TOP_RIGHT:
                    this.canvasRounder.setTopRightCornerRadius(cornerRadius);
                    break;
                case BOTTOM_RIGHT:
                    this.canvasRounder.setBottomRightCornerRadius(cornerRadius);
                    break;
                case BOTTOM_LEFT:
                    this.canvasRounder.setBottomLeftCornerRadius(cornerRadius);
            }

            Extensions.updateOutlineProvider(this, cornerRadius);
            this.invalidate();
        }
    }

}

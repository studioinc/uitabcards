package com.medstudioinc.uicard.listener;


import com.medstudioinc.uicard.tabs.UITabsCard;

public interface UITabsCardListener {
	 void onTabSelected(UITabsCard tab);
	
	 void onTabReselected(UITabsCard tab);
	
	 void onTabUnselected(UITabsCard tab);
}

package com.medstudioinc.uicard.listener;

import android.view.MotionEvent;
import android.view.View;

public interface OnCardViewTouchListener {
    boolean onTouch(View v, MotionEvent event);
}

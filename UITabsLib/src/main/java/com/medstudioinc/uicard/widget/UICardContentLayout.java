package com.medstudioinc.uicard.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.ColorRes;
import android.support.annotation.FloatRange;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;

import com.medstudioinc.uicard.utils.Utils;

public class UICardContentLayout extends ConstraintLayout {
    private GradientDrawable drawable;
    private int accentColor;
    private float roundedCornerRadius;

    public UICardContentLayout(Context context) {
        super(context);
    }

    public UICardContentLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public UICardContentLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public void setUICardRoundedCornerRadius(int radius){
        DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        roundedCornerRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, radius, metrics);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBackgroundColor(String color){
        GradientDrawable gd = new GradientDrawable();
        gd.setColor(Color.parseColor(color));
        gd.setCornerRadius(roundedCornerRadius);
        setBackgroundDrawable(gd);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBackgroundColor(int color){
        GradientDrawable gd = new GradientDrawable();
        gd.setColor(color);
        gd.setCornerRadius(roundedCornerRadius);
        setBackgroundDrawable(gd);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardGradientColors(String startColor, String endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] {Color.parseColor(startColor), Color.parseColor(endColor)});
        gd.setCornerRadius(roundedCornerRadius);
        setBackgroundDrawable(gd);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, String startColor, String endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[] {
                Color.parseColor(Utils.setColorAlpha(100, startColor)),
                Color.parseColor(Utils.setColorAlpha(100, endColor))
        });
        gd.setCornerRadius(roundedCornerRadius);
        setBackgroundDrawable(gd);
        setAlpha(alpha);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, @ColorRes int startColor, @ColorRes int endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[] {
                startColor,
                endColor
        });
        gd.setCornerRadius(roundedCornerRadius);
        setBackgroundDrawable(gd);
        setAlpha(alpha);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardGradientColors(String startColor, String centerColor, String endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] {
                Color.parseColor(startColor),
                Color.parseColor(centerColor),
                Color.parseColor(endColor)
        });
        gd.setCornerRadius(roundedCornerRadius);
        setBackgroundDrawable(gd);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, String startColor, String centerColor, String endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] {
                Color.parseColor(Utils.setColorAlpha(100, startColor)),
                Color.parseColor(Utils.setColorAlpha(100, centerColor)),
                Color.parseColor(Utils.setColorAlpha(100, endColor))
        });
        gd.setCornerRadius(roundedCornerRadius);
        setBackgroundDrawable(gd);
        setAlpha(alpha);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, @ColorRes int startColor, @ColorRes int centerColor, @ColorRes int endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] {
                startColor,
                centerColor,
                endColor
        });
        gd.setCornerRadius(roundedCornerRadius);
        setBackgroundDrawable(gd);
        setAlpha(alpha);
    }


    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, ColorStateList[] colorStateList){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[] {
                colorStateList[0].getDefaultColor(),
                colorStateList[1].getDefaultColor()
        });
        gd.setCornerRadius(roundedCornerRadius);
        setBackgroundDrawable(gd);
        setAlpha(alpha);
    }

    public void setUICardGradientViewFilter(boolean filter, String color){
        final int semiTransparentGrey = Color.argb(200, 185, 185, 255);
        PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(semiTransparentGrey, PorterDuff.Mode.SRC_ATOP);
        Paint paint = new Paint();
        paint.setColorFilter(colorFilter);
        setGrayedOut(filter, paint);
    }

    public void setGrayedOut(boolean grayedOut, Paint grayscalePaint) {
        if (grayedOut) {
            setLayerType(View.LAYER_TYPE_HARDWARE, grayscalePaint);
        } else {
            setLayerType(View.LAYER_TYPE_NONE, null);
        }
    }

    /**
     * Set the layout parameters associated with this view. These supply
     * parameters to the <i>parent</i> of this view specifying how it should be
     * arranged. There are many subclasses of ViewGroup.LayoutParams, and these
     * correspond to the different subclasses of ViewGroup that are responsible
     * for arranging their children.
     *
     * @param params The layout parameters for this view, cannot be null
     */
    public void setUICardContentLayoutParams(ViewGroup.LayoutParams params){
        setLayoutParams(params);
    }

}

package com.medstudioinc.uicard.cards;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.FloatRange;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;

import com.medstudioinc.uicard.listener.OnCardViewClickListener;
import com.medstudioinc.uicard.listener.OnCardViewTouchListener;
import com.medstudioinc.uicard.listener.OnLongCardViewClickListener;
import com.medstudioinc.uicard.utils.CornerType;
import com.medstudioinc.uicard.utils.UICardAnimation;
import com.medstudioinc.uicard.utils.Utils;
import com.medstudioinc.uicard.widget.UICardBottomSectionBar;
import com.medstudioinc.uicard.widget.UICardContentLayout;
import com.medstudioinc.uicard.widget.UICardGradientView;
import com.medstudioinc.uicard.widget.UICardImageView;
import com.medstudioinc.uicard.widget.UICardProgressBar;
import com.medstudioinc.uicard.widget.UICardRippleLayout;
import com.medstudioinc.uicard.widget.UICardRootLayout;
import com.medstudioinc.uicard.widget.UICardRoundedLayout;
import com.medstudioinc.uicard.widget.UICardTextView;
import com.medstudioinc.uitabslib.R;

public class UICardAllViews extends UICardRootLayout {
    private final String TAG = this.getClass().getName();

    boolean isBottomSection;
    boolean isFirstStart;
    boolean isSecondStart;

    public int margins = 5;
    public int width;
    public int height;

    UICardTextView tvUICardTitle;
    UICardImageView ivUICardThumbnail;
    UICardContentLayout uiCardContentLayout;
    UICardRoundedLayout uiCardRoundedLayout;
    UICardRippleLayout uiCardRippleLayout;
    UICardGradientView uiCardGradientView;
    UICardBottomSectionBar uiCardBottomSectionBar;
    UICardProgressBar uiCardProgressBar;


    OnLongCardViewClickListener longCardViewClickListener;
    OnCardViewClickListener cardViewClickListener;
    OnCardViewTouchListener cardViewTouchListener;

    public UICardAllViews(Context context, boolean isBottomSection) {
        super(context);
        this.isBottomSection = isBottomSection;
        init();
    }

    public UICardAllViews(Context context) {
        super(context);
        init();
    }

    public UICardAllViews(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UICardAllViews(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    public boolean isInEditMode() {
        return true;
    }

    private void init(){

        uiCardRippleLayout = new UICardRippleLayout(getContext());
        uiCardRippleLayout.setId(R.id.card_ripple_layout);
        ConstraintLayout.LayoutParams rippleParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        uiCardRippleLayout.setLayoutParams(rippleParams);


        /*uiCardProgressBar = new UICardProgressBar(getContext()); //, android.R.style.Widget_Material_ProgressBar
        ConstraintLayout.LayoutParams pbParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        pbParams.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
        pbParams.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID;
        pbParams.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID;
        pbParams.rightToRight = ConstraintLayout.LayoutParams.PARENT_ID;
        uiCardProgressBar.setLayoutParams(pbParams);
*/

        /*DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        float cornerRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, 0, metrics);
        GradientDrawable gd = new GradientDrawable();
        gd.setColor(Color.parseColor(Utils.setColorAlpha(30, "#F2F1F6")));
        gd.setCornerRadius(cornerRadius);
        uiCardRippleLayout.setBackgroundDrawable(gd);*/


        uiCardContentLayout = new UICardContentLayout(getContext());
        UICardContentLayout.LayoutParams countentLayoutParams = new UICardContentLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        uiCardContentLayout.setUICardContentLayoutParams(countentLayoutParams);

        uiCardRoundedLayout = new UICardRoundedLayout(getContext());
        uiCardRoundedLayout.setId(R.id.rounded_corner_view);
        UICardRoundedLayout.LayoutParams roundedCornerParams = new UICardRoundedLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        uiCardRoundedLayout.setLayoutParams(roundedCornerParams);
        uiCardRoundedLayout.setBackgroundColor(Color.DKGRAY);


        ivUICardThumbnail = new UICardImageView(getContext());
        ivUICardThumbnail.setId(R.id.card_thumbnail);
        ConstraintLayout.LayoutParams ivThumbnailParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        ivUICardThumbnail.setUICardThumbnailLayoutParams(ivThumbnailParams);


        uiCardBottomSectionBar = new UICardBottomSectionBar(getContext());
        uiCardBottomSectionBar.setId(R.id.section_view);
        ConstraintLayout.LayoutParams sectionParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
        sectionParams.matchConstraintDefaultHeight = ConstraintLayout.LayoutParams.MATCH_CONSTRAINT_WRAP;
        sectionParams.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID;
        sectionParams.height = 6;
        uiCardBottomSectionBar.setUICardSectionViewLayoutParams(sectionParams);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            uiCardBottomSectionBar.setElevation(15.0f);
        }

        uiCardGradientView = new UICardGradientView(getContext());
        uiCardGradientView.setUICardGradientViewLayoutParams(getUICardGradientViewLayoutParams());

        tvUICardTitle = new UICardTextView(getContext());
        ConstraintLayout.LayoutParams txtParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        txtParams.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
        txtParams.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID;
        txtParams.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID;
        txtParams.rightToRight = ConstraintLayout.LayoutParams.PARENT_ID;
        tvUICardTitle.setUICardTextLayoutParams(txtParams);
        tvUICardTitle.setUICardTextId(R.id.card_title);


        isInEditMode();
        defaultValues();
        setUpDimentions();

        addViews();

        setUpListeners();
    }


    private void addViews(){
        post(()->{
            addView(uiCardRippleLayout, getUICardRootLayoutParams());
        });

        uiCardRippleLayout.addView(uiCardRoundedLayout);
        uiCardRoundedLayout.addView(ivUICardThumbnail);
        uiCardRoundedLayout.addView(uiCardGradientView);
        if (isBottomSection){
            uiCardRoundedLayout.addView(uiCardBottomSectionBar);
        }
        uiCardRoundedLayout.addView(tvUICardTitle);
    }

    private void setUpListeners(){
        uiCardRippleLayout.setOnClickListener(v-> {
            if (cardViewClickListener != null) {
                UICardAnimation.setup(this).commit(new UICardAnimation.UICardAnimationListener() {
                    @Override
                    public void onAnimationEnd() {
                        if (cardViewClickListener != null){
                            cardViewClickListener.onClick(v);
                        }
                    }
                });
            }
        });
        uiCardRippleLayout.setOnLongClickListener(v-> {
            if (longCardViewClickListener != null){
                UICardAnimation.setup(this).commit(new UICardAnimation.UICardAnimationListener() {
                    @Override
                    public void onAnimationEnd() {
                        if (longCardViewClickListener != null){
                            longCardViewClickListener.onLongClick(v);
                        }
                    }
                });
            }
            return false;
        });
        uiCardRippleLayout.setOnTouchListener((v, e)->{
            if (cardViewTouchListener != null){
                cardViewTouchListener.onTouch(v, e);
            }
            return false;
        });
    }


    private void setUpDimentions(){
        if (!isFirstStart){
            this.post(()->{
                int finalWidth = this.getMeasuredWidth();
                height = (int) (finalWidth / 1.356f);
                isFirstStart = true;
            });
        }
        if (!isSecondStart){
            this.post(()->{
                int finalWidth = this.getMeasuredWidth();
                height = (int) (finalWidth / 1.356f);
                this.post(()->{
                    isSecondStart = true;
                });
                Log.d(TAG, "Width: " + finalWidth + "\nHeight: " + height);
            });
        }
    }

    private void defaultValues(){
        uiCardRippleLayout.setRippleDelayClick(true);
        uiCardRippleLayout.setRippleOverlay(true);
        uiCardRippleLayout.setRippleHover(true);
        uiCardRippleLayout.setRippleAlpha(1);
        uiCardRippleLayout.setRippleColor(Color.parseColor("#E9EBEE"));

        tvUICardTitle.setUICardTextShadowLayer(5, -2, -2, Color.DKGRAY);
        tvUICardTitle.setUICardTextColor(Color.WHITE);
        tvUICardTitle.setUICardTextFont("fonts/future.ttf");
        tvUICardTitle.setUICardTextSize(13);
    }


    public ConstraintLayout.LayoutParams getUICardRootLayoutParams(){
        ConstraintLayout.LayoutParams viewParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        viewParams.width =  getUICardWidth();
        viewParams.height = getUICardHeight();
        return viewParams;
    }

    private ConstraintLayout.LayoutParams getUICardGradientViewLayoutParams(){
        ConstraintLayout.LayoutParams params;
        if (isBottomSection){
            params = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
            params.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
            params.bottomToTop = R.id.section_view;
            params.matchConstraintDefaultHeight = ConstraintLayout.LayoutParams.MATCH_CONSTRAINT_SPREAD;
        } else {
            params = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
        return params;
    }


    public int getUICardMargins() {
        return this.margins;
    }

    public void setUICardMargins(int margins) {
        this.margins = margins;
    }

    public int getUICardWidth() {
        return ViewGroup.LayoutParams.MATCH_PARENT;
    }

    private void setUICardWidth(int width) {
        this.width = width;
    }

    public int getUICardHeight() {
        return height;
    }

    public void setUICardHeight(int height) {
        this.height = height;
    }

    public void setBottomSectionBar(boolean section){
        this.isBottomSection = section;
    }

    public void setUICardRoundedCornerRadius(int radius, CornerType cornerType){
        if (radius > 0){
            uiCardRippleLayout.setUICardRippleRoundedCorners(radius);
            uiCardRoundedLayout.setUICardRoundedCornerRadius(radius, cornerType);
        }
    }

    /**
     * Sets the identifier for this view. The identifier does not have to be
     * unique in this view's hierarchy. The identifier should be a positive
     * number.
     *
     * @see #NO_ID
     * @see #getId()
     * @see #findViewById(int)
     *
     * @param id a number used to identify the view
     *
     * @attr ref android.R.styleable#View_id
     */
    public void setUICardTextId(@IdRes int id){
        tvUICardTitle.setId(id);
    }

    /**
     * Sets the text to be displayed using a string resource identifier.
     *
     * @param resid the resource identifier of the string resource to be displayed
     *
     * @see #@setText(CharSequence)
     *
     * @attr ref android.R.styleable#TextView_text
     */
    public void setUICardText(@StringRes int resid){
        tvUICardTitle.setText(getContext().getResources().getString(resid));
    }

    /**
     * Sets the text to be displayed. TextView <em>does not</em> accept
     * HTML-like formatting, which you can do with text strings in XML resource files.
     * To style your strings, attach android.text.style.* objects to a
     * {@link android.text.SpannableString}, or see the
     * <a href="{@docRoot}guide/topics/resources/available-resources.html#stringresources">
     * Available Resource Types</a> documentation for an example of setting
     * formatted text in the XML resource file.
     * <p/>
     * When required, TextView will use {@link android.text.Spannable.Factory} to create final or
     * intermediate {@link @Spannable Spannables}. Likewise it will use
     * {@link android.text.Editable.Factory} to create final or intermediate
     * {@link @Editable Editables}.
     *
     * If the passed text is a {@link @PrecomputedText} but the parameters used to create the
     * PrecomputedText mismatches with this TextView, IllegalArgumentException is thrown. To ensure
     * the parameters match, you can call {@link @TextView#setTextMetricsParams} before calling this.
     *
     * @param text text to be displayed
     *
     * @attr ref android.R.styleable#TextView_text
     * @throws IllegalArgumentException if the passed text is a {@link @PrecomputedText} but the
     *                                  parameters used to create the PrecomputedText mismatches
     *                                  with this TextView.
     */
    public void setUICardText(CharSequence text){
        tvUICardTitle.setText(text);
    }

    /**
     * Set the default text size to the given value, interpreted as "scaled
     * pixel" units.  This size is adjusted based on the current density and
     * user font size preference.
     *
     * <p>Note: if this TextView has the auto-size feature enabled than this function is no-op.
     *
     * @param size The scaled pixel size.
     *
     * @attr ref android.R.styleable#TextView_textSize
     */
    public void setUICardTextSize(int size){
        tvUICardTitle.setTextSize(size);
    }


    /**
     * Set the default text size to a given unit and value. See {@link
     * TypedValue} for the possible dimension units.
     *
     * <p>Note: if this TextView has the auto-size feature enabled than this function is no-op.
     *
     * @param unit The desired dimension unit.
     * @param size The desired size in the given units.
     *
     * @attr ref android.R.styleable#TextView_textSize
     */
    public void setUICardTextSize(int unit, int size){
        tvUICardTitle.setTextSize(unit, size);
    }


    /**
     * Sets the text color for all the states (normal, selected,
     * focused) to be this color.
     *
     * @param color A color value in the form 0xAARRGGBB.
     * Do not pass a resource ID. To get a color value from a resource ID, call
     * {@link android.support.v4.content.ContextCompat#getColor(Context, int) getColor}.
     *
     * @see #@setTextColor(ColorStateList)
     * @see #@getTextColors()
     *
     * @attr ref android.R.styleable#TextView_textColor
     */
    public void setUICardTextColor(int color){
        tvUICardTitle.setTextColor(color);
    }

    /**
     * Sets the text color.
     *
     * @see #@setTextColor(int)
     * @see #@getTextColors()
     * @see #@setHintTextColor(ColorStateList)
     * @see #@setLinkTextColor(ColorStateList)
     *
     * @attr ref android.R.styleable#TextView_textColor
     */
    public void setUICardTextColor(ColorStateList color){
        tvUICardTitle.setTextColor(color);
    }


    /**
     * Sets the typeface and style in which the text should be displayed.
     * Note that not all Typeface families actually have bold and italic
     * variants, so you may need to use
     * {@link #@setTypeface(Typeface, int)} to get the appearance
     * that you actually want.
     *
     * @see #@getTypeface()
     *
     * @attr ref android.R.styleable#TextView_fontFamily
     * @attr ref android.R.styleable#TextView_typeface
     * @attr ref android.R.styleable#TextView_textStyle
     */
    public void setUICardTextFont(Typeface typeface){
        tvUICardTitle.setTypeface(typeface);
    }


    /**
     * Sets the typeface and style in which the text should be displayed.
     * Note that not all Typeface families actually have bold and italic
     * variants, so you may need to use
     * {@link #@setTypeface(Typeface, int)} to get the appearance
     * that you actually want.
     *
     * @see #@getTypeface()
     *
     * @attr ref android.R.styleable#TextView_fontFamily
     * @attr ref android.R.styleable#TextView_typeface
     * @attr ref android.R.styleable#TextView_textStyle
     */
    public void setUICardTextFont(String typefacePath){
        if (typefacePath != null){
            Typeface fromAsset = Typeface.createFromAsset(getContext().getAssets(), typefacePath);
            tvUICardTitle.setTypeface(fromAsset);
        }
    }

    /**
     * Gives the text a shadow of the specified blur radius and color, the specified
     * distance from its drawn position.
     * <p>
     * The text shadow produced does not interact with the properties on view
     * that are responsible for real time shadows,
     * {@link View#getElevation() elevation} and
     * {@link View#getTranslationZ() translationZ}.
     *
     * @see @Paint#setShadowLayer(float, float, float, int)
     *
     * @attr ref android.R.styleable#TextView_shadowColor
     * @attr ref android.R.styleable#TextView_shadowDx
     * @attr ref android.R.styleable#TextView_shadowDy
     * @attr ref android.R.styleable#TextView_shadowRadius
     */
    public void setUICardTextShadowLayer(float radius, float dx, float yx, int color){
        tvUICardTitle.setShadowLayer(radius, dx, yx, color);
    }


    /**
     * Sets a drawable as the content of this ImageView.
     * <p class="note">This does Bitmap reading and decoding on the UI
     * thread, which can cause a latency hiccup.  If that's a concern,
     * consider using {@link #@setImageDrawable(android.graphics.drawable.Drawable)} or
     * {@link #@setImageBitmap(android.graphics.Bitmap)} and
     * {@link android.graphics.BitmapFactory} instead.</p>
     *
     * @param resId the resource identifier of the drawable
     *
     * @attr ref android.R.styleable#ImageView_src
     */
    public void setUICardThumbnail(@DrawableRes int resId){
        ivUICardThumbnail.setUICardThumbnail(resId);
    }

    /**
     * Sets a drawable as the content of this ImageView.
     *
     * @param drawable the Drawable to set, or {@code null} to clear the
     *                 content
     */
    public void setUICardThumbnail(@Nullable Drawable drawable){
        ivUICardThumbnail.setUICardThumbnail(drawable);
    }


    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardGradientColors(String startColor, String endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] {Color.parseColor(startColor), Color.parseColor(endColor)});
        uiCardGradientView.setBackgroundDrawable(gd);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, String startColor, String endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[] {
                Color.parseColor(Utils.setColorAlpha(100, startColor)),
                Color.parseColor(Utils.setColorAlpha(100, endColor))
        });
        uiCardGradientView.setBackgroundDrawable(gd);
        uiCardGradientView.setAlpha(alpha);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, @ColorRes int startColor, @ColorRes int endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[] {
                startColor,
                endColor
        });
        uiCardGradientView.setBackgroundDrawable(gd);
        uiCardGradientView.setAlpha(alpha);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardGradientColors(String startColor, String centerColor, String endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] {
                Color.parseColor(startColor),
                Color.parseColor(centerColor),
                Color.parseColor(endColor)
        });
        uiCardGradientView.setBackgroundDrawable(gd);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, String startColor, String centerColor, String endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] {
                Color.parseColor(Utils.setColorAlpha(100, startColor)),
                Color.parseColor(Utils.setColorAlpha(100, centerColor)),
                Color.parseColor(Utils.setColorAlpha(100, endColor))
        });
        uiCardGradientView.setBackgroundDrawable(gd);
        uiCardGradientView.setAlpha(alpha);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, @ColorRes int startColor, @ColorRes int centerColor, @ColorRes int endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] {
                startColor,
                centerColor,
                endColor
        });
        uiCardGradientView.setBackgroundDrawable(gd);
        uiCardGradientView.setAlpha(alpha);
    }


    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, ColorStateList[] colorStateList){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[] {
                colorStateList[0].getDefaultColor(),
                colorStateList[1].getDefaultColor()
        });
        uiCardGradientView.setBackgroundDrawable(gd);
        uiCardGradientView.setAlpha(alpha);
    }

    public void setUICardGradientViewFilter(boolean filter, String color){
        final int semiTransparentGrey = Color.argb(200, 185, 185, 255);
        PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(semiTransparentGrey, PorterDuff.Mode.SRC_ATOP);
        Paint paint = new Paint();
        paint.setColorFilter(colorFilter);
        uiCardGradientView.setGrayedOut(filter, paint);
    }


    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionBackgroundColor(String color){
        uiCardBottomSectionBar.setBackgroundColor(Color.parseColor(color));
    }


    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionBackgroundColor(@FloatRange(from = 0, to = 1.0) float alpha, String color){
        uiCardBottomSectionBar.setBackgroundColor(Color.parseColor(color));
        uiCardBottomSectionBar.setAlpha(alpha);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionBackgroundColor(@ColorRes int resid){
        uiCardBottomSectionBar.setBackgroundColor(getContext().getResources().getColor(resid));
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionBackgroundColor(@FloatRange(from = 0, to = 1.0) float alpha, @ColorRes int resid){
        uiCardBottomSectionBar.setBackgroundColor(getContext().getResources().getColor(resid));
        uiCardBottomSectionBar.setAlpha(alpha);
    }


    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionGradientColors(String startColor, String endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] {Color.parseColor(startColor), Color.parseColor(endColor)});
        uiCardBottomSectionBar.setBackgroundDrawable(gd);
    }


    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, String startColor, String endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[] {
                Color.parseColor(Utils.setColorAlpha(100, startColor)),
                Color.parseColor(Utils.setColorAlpha(100, endColor))
        });
        gd.setCornerRadius(2);
        uiCardBottomSectionBar.setBackgroundDrawable(gd);
        uiCardBottomSectionBar.setAlpha(alpha);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, @ColorRes int startColor, @ColorRes int endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[] {
                startColor,
                endColor
        });
        uiCardBottomSectionBar.setBackgroundDrawable(gd);
        uiCardBottomSectionBar.setAlpha(alpha);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionGradientColors(String startColor, String centerColor, String endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] {
                Color.parseColor(startColor),
                Color.parseColor(centerColor),
                Color.parseColor(endColor)
        });
        uiCardBottomSectionBar.setBackgroundDrawable(gd);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, String startColor, String centerColor, String endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] {
                Color.parseColor(Utils.setColorAlpha(100, startColor)),
                Color.parseColor(Utils.setColorAlpha(100, centerColor)),
                Color.parseColor(Utils.setColorAlpha(100, endColor))
        });
        uiCardBottomSectionBar.setBackgroundDrawable(gd);
        uiCardBottomSectionBar.setAlpha(alpha);
    }

    /**
     * @deprecated use {@link #setBackground(Drawable)} instead
     */
    public void setUICardBottomSectionGradientColors(@FloatRange(from = 0, to = 1.0) float alpha, @ColorRes int startColor, @ColorRes int centerColor, @ColorRes int endColor){
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] {
                startColor,
                centerColor,
                endColor
        });
        uiCardBottomSectionBar.setBackgroundDrawable(gd);
        uiCardBottomSectionBar.setAlpha(alpha);
    }

    public void setUICardBottomSectionViewFilter(boolean filter, String color){
        final int semiTransparentGrey = Color.argb(200, 185, 185, 255);
        PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(semiTransparentGrey, PorterDuff.Mode.SRC_ATOP);
        Paint paint = new Paint();
        paint.setColorFilter(colorFilter);
        uiCardBottomSectionBar.setGrayedOut(filter, paint);
    }

    /**
     * Register a callback to be invoked when this view is clicked. If this view is not
     * clickable, it becomes clickable.
     *
     * @param listener The callback that will run
     *
     * @see #setOnCardViewClickListener(OnCardViewClickListener)
     */
    public void setOnCardViewClickListener(OnCardViewClickListener listener){
        this.cardViewClickListener = listener;
    }

    /**
     * Register a callback to be invoked when this view is clicked and held. If this view is not
     * long clickable, it becomes long clickable.
     *
     * @param listener The callback that will run
     *
     * @see #setOnCardViewLongClickListener(OnLongCardViewClickListener)
     */
    public void setOnCardViewLongClickListener(OnLongCardViewClickListener listener){
        this.longCardViewClickListener = listener;
    }

    /**
     * Register a callback to be invoked when this view is clicked and held. If this view is not
     * long touchable, it becomes long touchable.
     *
     * @param listener The callback that will run
     *
     * @see #setOnCardViewTouchListener(OnCardViewTouchListener)
     */

    public void setOnCardViewTouchListener(OnCardViewTouchListener listener){
        this.cardViewTouchListener = listener;
    }

}

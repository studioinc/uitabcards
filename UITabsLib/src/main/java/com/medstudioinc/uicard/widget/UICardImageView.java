package com.medstudioinc.uicard.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.RequestOptions;

@SuppressLint("AppCompatCustomView")
public class UICardImageView extends ImageView {
    public UICardImageView(Context context) {
        super(context);
    }

    public UICardImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public UICardImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Sets the identifier for this view. The identifier does not have to be
     * unique in this view's hierarchy. The identifier should be a positive
     * number.
     *
     * @see #NO_ID
     * @see #getId()
     * @see #findViewById(int)
     *
     * @param id a number used to identify the view
     *
     * @attr ref android.R.styleable#View_id
     */
    public void setUICardThumbnailId(@IdRes int id){
        setId(id);
    }


    /**
     * Sets a drawable as the content of this ImageView.
     * <p class="note">This does Bitmap reading and decoding on the UI
     * thread, which can cause a latency hiccup.  If that's a concern,
     * consider using {@link #setImageDrawable(android.graphics.drawable.Drawable)} or
     * {@link #setImageBitmap(android.graphics.Bitmap)} and
     * {@link android.graphics.BitmapFactory} instead.</p>
     *
     * @param resId the resource identifier of the drawable
     *
     * @attr ref android.R.styleable#ImageView_src
     */
    public void setUICardThumbnail(@DrawableRes int resId){
        loadThumbnail(resId).into(this);
    }

    /**
     * Sets a drawable as the content of this ImageView.
     *
     * @param drawable the Drawable to set, or {@code null} to clear the
     *                 content
     */
    public void setUICardThumbnail(@Nullable Drawable drawable){
        loadThumbnail(drawable).into(this);
    }



    /**
     * Set the layout parameters associated with this view. These supply
     * parameters to the <i>parent</i> of this view specifying how it should be
     * arranged. There are many subclasses of ViewGroup.LayoutParams, and these
     * correspond to the different subclasses of ViewGroup that are responsible
     * for arranging their children.
     *
     * @param params The layout parameters for this view, cannot be null
     */
    public void setUICardThumbnailLayoutParams(ViewGroup.LayoutParams params){
        setLayoutParams(params);
    }

    /**
     * Provides type independent options to customize loads with Glide.
     */
    public RequestOptions requestOptions(){
        return new RequestOptions();
    }


    /**
     * A helper method equivalent to calling {@link #requestOptions()} and then {@link
     * RequestBuilder#load(Object)} with the given model.
     *
     * @return A new request builder for loading a {@link Drawable} using the given model.
     */

    private RequestBuilder<Drawable> loadThumbnail(@Nullable Object file){
        return Glide.with(getContext()).load(file).apply(requestOptions().centerCrop());
    }

}

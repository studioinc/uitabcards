package com.medstudioinc.uicard.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.medstudioinc.uicard.utils.CornerType;

public class UICardRoundedLayout extends RoundCornerLayout { //ConstraintLayout
    /*private Bitmap mOffscreenBitmap;
    private Canvas mOffscreenCanvas;
    private BitmapShader mBitmapShader;
    private Paint mPaint;
    private RectF mRectF;

    private static float roundedCornersRadius;
    private static final int DEFAULT_ROUNDED_CORNERS = 0;

    private Paint paint, maskPaint;
    private float cornerRadius;
*/

    public UICardRoundedLayout(Context context) {
        super(context);
    }

    public UICardRoundedLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public UICardRoundedLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

       /* TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.UICardRoundedLayout, defStyleAttr, defStyleAttr);
        try {
            cornerRadius = typedArray.getDimension(R.styleable.UICardRoundedLayout_rounded_corner_radius, DEFAULT_ROUNDED_CORNERS);
        } finally {
            typedArray.recycle();
        }*/
    }


    public void setUICardRoundedCornerRadius(float radius, CornerType cornerType) {
        //this.roundedCornersRadius = radius;
        setCornerRadius(radius, cornerType);

        //DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        //cornerRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, roundedCornersRadius, metrics);
    }

    /*private void init() {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        maskPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
        mBitmapShader = new BitmapShader(mOffscreenBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        maskPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        setWillNotDraw(false);
    }

    @Override
    public void draw(Canvas canvas) {
        if (mOffscreenBitmap == null) {
            mOffscreenBitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
            mOffscreenCanvas = new Canvas(mOffscreenBitmap);
            mBitmapShader = new BitmapShader(mOffscreenBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            mPaint.setShader(mBitmapShader);
            mRectF = new RectF(0f, 0f, canvas.getWidth(), canvas.getHeight());
        }
        super.draw(mOffscreenCanvas);
        canvas.drawRoundRect(mRectF, cornerRadius, cornerRadius, mPaint);
    }*/


}
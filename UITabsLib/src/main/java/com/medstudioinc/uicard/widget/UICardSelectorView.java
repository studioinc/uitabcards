package com.medstudioinc.uicard.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;

@SuppressLint("AppCompatCustomView")
public class UICardSelectorView extends ImageView {
    public UICardSelectorView(Context context) {
        super(context);
    }

    public UICardSelectorView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public UICardSelectorView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    // Set layout background color
    public void setCardSelectorBackgroundColor(int color){
        setBackgroundColor(color);
    }
}

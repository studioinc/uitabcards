package com.medstudioinc.uicard.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public class UICardProgressBar extends ProgressBar {
    public UICardProgressBar(Context context) {
        super(context);
    }

    public UICardProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UICardProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
